export const ADD_TEAM_MEMBER_REQUEST = "ADD_TEAM_MEMBER_REQUEST";
export const ADD_TEAM_MEMBER_SUCCESS = "ADD_TEAM_MEMBER_SUCCESS";
export const ADD_TEAM_MEMBER_FAILURE = "ADD_TEAM_MEMBER_FAILURE";

export const addTeamMemberRequest = (teamMember) => ({
    type: ADD_TEAM_MEMBER_REQUEST,
    payload: {
        teamMember,
    },
});
export const addTeamMemberSuccess = (teamMemberId, entities) => ({
    type: ADD_TEAM_MEMBER_SUCCESS,
    payload: {
        entities,
        teamMemberId,
    },
});
export const addTeamMemberFailure = (error) => ({
    type: ADD_TEAM_MEMBER_FAILURE,
    payload: {
        error,
    },
});

export const UPDATE_TEAM_MEMBER_REQUEST = "UPDATE_TEAM_MEMBER_REQUEST";
export const UPDATE_TEAM_MEMBER_SUCCESS = "UPDATE_TEAM_MEMBER_SUCCESS";
export const UPDATE_TEAM_MEMBER_FAILURE = "UPDATE_TEAM_MEMBER_FAILURE";

export const updateTeamMemberRequest = (teamMember) => ({
    type: UPDATE_TEAM_MEMBER_REQUEST,
    payload: {
        teamMember,
    },
});
export const updateTeamMemberSuccess = (entities) => ({
    type: UPDATE_TEAM_MEMBER_SUCCESS,
    payload: {
        entities,
    },
});
export const updateTeamMemberFailure = (error) => ({
    type: UPDATE_TEAM_MEMBER_FAILURE,
    payload: {
        error,
    },
});

export const DELETE_TEAM_MEMBER_REQUEST = "DELETE_TEAM_MEMBER_REQUEST";
export const DELETE_TEAM_MEMBER_SUCCESS = "DELETE_TEAM_MEMBER_SUCCESS";
export const DELETE_TEAM_MEMBER_FAILURE = "DELETE_TEAM_MEMBER_FAILURE";

export const deleteTeamMemberRequest = (teamMember) => ({
    type: DELETE_TEAM_MEMBER_REQUEST,
    payload: {
        teamMember,
    },
});
export const deleteTeamMemberSuccess = (teamMemberId, entities) => ({
    type: DELETE_TEAM_MEMBER_SUCCESS,
    payload: {
        teamMemberId,
        entities,
    },
});
export const deleteTeamMemberFailure = (error) => ({
    type: DELETE_TEAM_MEMBER_FAILURE,
    payload: {
        error,
    },
});