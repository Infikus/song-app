export const FETCH_TEAM_REQUEST = 'FETCH_TEAM_REQUEST';
export const FETCH_TEAM_SUCCESS = 'FETCH_TEAM_SUCCESS';
export const FETCH_TEAM_FAILURE = 'FETCH_TEAM_FAILURE';

export const fetchTeamRequest = (teamId) => ({
    type: FETCH_TEAM_REQUEST,
    payload: {
        teamId,
    }
});
export const fetchTeamSuccess = (teamId, entities) => ({
    type: FETCH_TEAM_SUCCESS,
    payload: {
        entities,
        teamId,
    }
})
export const fetchTeamFailure = (error) => ({
    type: FETCH_TEAM_FAILURE,
    payload: {
        error,
    },
})

export const POST_TEAM_REQUEST = 'POST_TEAM_REQUEST';
export const POST_TEAM_SUCCESS = 'POST_TEAM_SUCCESS';
export const POST_TEAM_FAILURE = 'POST_TEAM_FAILURE';

export const postTeamRequest = (team) => ({
    type: POST_TEAM_REQUEST,
    payload: {
        team,
    }
});
export const postTeamSuccess = (teamId, entities) => ({
    type: POST_TEAM_SUCCESS,
    payload: {
        entities,
        teamId,
    }
})
export const postTeamFailure = (error) => ({
    type: POST_TEAM_FAILURE,
    payload: {
        error,
    },
})

export const UPDATE_TEAM_REQUEST = 'UPDATE_TEAM_REQUEST';
export const UPDATE_TEAM_SUCCESS = 'UPDATE_TEAM_SUCCESS';
export const UPDATE_TEAM_FAILURE = 'UPDATE_TEAM_FAILURE';

export const updateTeamRequest = (team) => ({
    type: UPDATE_TEAM_REQUEST,
    payload: {
        team,
    },
});
export const updateTeamSuccess = (entities) => ({
    type: UPDATE_TEAM_SUCCESS,
    payload: {
        entities,
    },
});
export const updateTeamFailure = (error) => ({
    type: UPDATE_TEAM_FAILURE,
    payload: {
        error,
    },
});

export const DELETE_TEAM_REQUEST = 'DELETE_TEAM_REQUEST';
export const DELETE_TEAM_SUCCESS = 'DELETE_TEAM_SUCCESS';
export const DELETE_TEAM_FAILURE = 'DELETE_TEAM_FAILURE';

export const deleteTeamRequest = (team) => ({
    type: DELETE_TEAM_REQUEST,
    payload: {
        team,
    },
});
export const deleteTeamSuccess = (teamId, entities) => ({
    type: DELETE_TEAM_SUCCESS,
    payload: {
        entities,
        teamId,
    },
});
export const deleteTeamFailure = (error) => ({
    type: DELETE_TEAM_FAILURE,
    payload: {
        error,
    },
});

export const SET_SELECTED_TEAM = 'SET_SELECTED_TEAM';

export const setSelectedTeam = (teamId) => ({
    type: SET_SELECTED_TEAM,
    payload: {
        teamId,
    }
})