import { FETCH_SONG_LIST_REQUEST } from './songLists.action';
import { FETCH_SONG_REQUEST } from './songs.action';

export const SUBSCRIBE = 'SUBSCRIBE';

export const subscribe = userId => ({
  type: SUBSCRIBE,
  payload: {
    userId,
  },
});

export const allowedActions = [
  FETCH_SONG_LIST_REQUEST, FETCH_SONG_REQUEST,
]
