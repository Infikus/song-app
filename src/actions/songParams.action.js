export const SET_SONG_PARAMS_REQUEST = 'SET_SONG_PARAMS_REQUEST';

export const setSongParamsRequest = (songParams, song, songListItem=null) => ({
  type: SET_SONG_PARAMS_REQUEST,
  payload: {
    songParams,
    song,
    songListItem,
  },
});

export const SET_SONG_PARAMS_SUCCESS = 'SET_SONG_PARAMS_SUCCESS';

export const setSongParamsSuccess = (songParamsId, entities) => ({
  type: SET_SONG_PARAMS_SUCCESS,
  payload: {
    entities,
    songParamsId,
  }
});

export const SET_SONG_PARAMS_FAILURE = 'SET_SONG_PARAMS_FAILURE';

export const setSongParamsFailure = (error) => ({
  type: SET_SONG_PARAMS_FAILURE,
  payload: {
    error,
  }
});
