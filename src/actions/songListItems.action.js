export const UPDATE_SONG_LIST_ITEM_REQUEST = 'UPDATE_SONG_LIST_ITEM_REQUEST';

export const updateSongListItemRequest = (songListItem) => ({
  type: UPDATE_SONG_LIST_ITEM_REQUEST,
  payload: {
    songListItem,
  },
});

export const UPDATE_SONG_LIST_ITEM_SUCCESS = 'UPDATE_SONG_LIST_ITEM_SUCCESS';

export const updateSongListItemSuccess = (songListItemId, entities) => ({
  type: UPDATE_SONG_LIST_ITEM_SUCCESS,
  payload: {
    songListItemId,
    entities,
  },
});

export const UPDATE_SONG_LIST_ITEM_FAILURE = 'UPDATE_SONG_LIST_ITEM_FAILURE';

export const updateSongListItemFailure = (error) => ({
  type: UPDATE_SONG_LIST_ITEM_FAILURE,
  payload: {
    error,
  },
});
