import moment from 'moment';

export const FETCH_SONG_LISTS_REQUEST = 'FETCH_SONG_LISTS_REQUEST';
export const FETCH_SONG_LISTS_SUCCESS = 'FETCH_SONG_LISTS_SUCCESS';
export const FETCH_SONG_LISTS_FAILURE = 'FETCH_SONG_LISTS_FAILURE';

export const getSongLists = () => ({
  type: FETCH_SONG_LISTS_REQUEST,
  payload: {},
});
export const getSongListsSuccess = (entities, songListsId) => ({
  type: FETCH_SONG_LISTS_SUCCESS,
  payload: {
    entities,
    songListsId,
  },
});
export const getSongListsFailure = (error) => ({
  type: FETCH_SONG_LISTS_FAILURE,
  payload: {
    error,
  },
});

export const FETCH_SONG_LIST_REQUEST = 'FETCH_SONG_LIST_REQUEST';
export const FETCH_SONG_LIST_SUCCESS = 'FETCH_SONG_LIST_SUCCESS';
export const FETCH_SONG_LIST_FAILURE = 'FETCH_SONG_LIST_FAILURE';

/* Action creators */
export const getSongList = (songListId) => ({
  type: FETCH_SONG_LIST_REQUEST,
  payload: {
    songListId,
  },
});
export const getSongListSuccess = (entities) => ({
  type: FETCH_SONG_LIST_SUCCESS,
  payload: {
    entities,
  }
});
export const getSongListFailure = (error) => ({
  type: FETCH_SONG_LIST_FAILURE,
  payload: {
    error,
  },
});

export const POST_SONG_LIST_REQUEST = 'POST_SONG_LIST_REQUEST';
export const POST_SONG_LIST_SUCCESS = 'POST_SONG_LIST_SUCCESS';
export const POST_SONG_LIST_FAILURE = 'POST_SONG_LIST_FAILURE';

export const postSongList = (songList) => {
  const date = moment(songList.date).startOf('day').format();
  return {
    type: POST_SONG_LIST_REQUEST,
    payload: {
      songList: Object.assign({}, songList, {
        date
      }),
    },
}};
export const postSongListSuccess = (songListId, entities) => ({
  type: POST_SONG_LIST_SUCCESS,
  payload: {
    entities,
    songListId,
  },
});
export const postSongListFailure = (error) => ({
  type: POST_SONG_LIST_FAILURE,
  payload: {
    error,
  },
});

export const UPDATE_SONG_LIST_REQUEST = 'UPDATE_SONG_LIST_REQUEST';
export const UPDATE_SONG_LIST_SUCCESS = 'UPDATE_SONG_LIST_SUCCESS';
export const UPDATE_SONG_LIST_FAILURE = 'UPDATE_SONG_LIST_FAILURE';

export const updateSongList = (songList) => {
  const date = moment(songList.date).startOf('day').format();
  return {
    type: UPDATE_SONG_LIST_REQUEST,
    payload: {
      songList: Object.assign({}, songList, {
        date
      }),
    },
}};
export const updateSongListSuccess = (entities) => ({
  type: UPDATE_SONG_LIST_SUCCESS,
  payload: {
    entities,
  },
});
export const updateSongListFailure = (error) => ({
  type: UPDATE_SONG_LIST_FAILURE,
  payload: {
    error,
  },
});

export const DELETE_SONG_LIST_REQUEST = 'DELETE_SONG_LIST_REQUEST';
export const DELETE_SONG_LIST_SUCCESS = 'DELETE_SONG_LIST_SUCCESS';
export const DELETE_SONG_LIST_FAILURE = 'DELETE_SONG_LIST_FAILURE';

export const deleteSongListRequest = (songList) => ({
  type: DELETE_SONG_LIST_REQUEST,
  payload: {
    songList,
  },
});
export const deleteSongListSuccess = (songListId, entities) => ({
  type: DELETE_SONG_LIST_SUCCESS,
  payload: {
    songListId,
    entities,
  }
});
export const deleteSongListFailure = (error) => ({
  type: DELETE_SONG_LIST_FAILURE,
  payload: {
    error,
  },
});
