export const FETCH_SONGS_REQUEST = 'FETCH_SONGS_REQUEST';
export const FETCH_SONGS_SUCCESS = 'FETCH_SONGS_SUCCESS';
export const FETCH_SONGS_FAILURE = 'FETCH_SONGS_FAILURE';

/* Action creators */
export const getSongs = () => ({
    type: FETCH_SONGS_REQUEST,
    payload: {}
});
export const getSongsSuccess = (songsId, entities) => ({
    type: FETCH_SONGS_SUCCESS,
    payload: {
        entities,
        songsId,
    }
});
export const getSongsFailure = (error) => ({
    type: FETCH_SONGS_FAILURE,
    error
});

export const FETCH_SONG_REQUEST = 'FETCH_SONG_REQUEST';
export const FETCH_SONG_SUCCESS = 'FETCH_SONG_SUCCESS';
export const FETCH_SONG_FAILURE = 'FETCH_SONG_FAILURE';

/* Action creators */
export const getSong = (songId) => ({
    type: FETCH_SONG_REQUEST,
    payload: {
        songId,
    }
});
export const getSongSuccess = (id, entities) => ({
    type: FETCH_SONG_SUCCESS,
    payload: {
        entities,
        id,
    }
});
export const getSongFailure = (error) => ({
    type: FETCH_SONG_FAILURE,
    payload: {
        error,
    }
});

export const POST_SONG_REQUEST = 'POST_SONG_REQUEST';
export const POST_SONG_SUCCESS = 'POST_SONG_SUCCESS';
export const POST_SONG_FAILURE = 'POST_SONG_FAILURE';

export const postSong = (song) => ({
    type: POST_SONG_REQUEST,
    payload: {
        song,
    }
});
export const postSongSuccess = (songId, entities) => ({
    type: POST_SONG_SUCCESS,
    payload: {
        songId,
        entities,
    }
});
export const postSongFailure = (error) => ({
    type: POST_SONG_FAILURE,
    payload: {
        error,
    }
});

export const UPDATE_SONG_REQUEST = 'UPDATE_SONG_REQUEST';
export const UPDATE_SONG_SUCCESS = 'UPDATE_SONG_SUCCESS';
export const UPDATE_SONG_FAILURE = 'UPDATE_SONG_FAILURE';

export const updateSong = (song) => ({
    type: UPDATE_SONG_REQUEST,
    payload: {
        song,
    }
});
export const updateSongSuccess = (entities) => ({
    type: UPDATE_SONG_SUCCESS,
    payload: {
        entities,
    }
});
export const updateSongFailure = (error) => ({
    type: UPDATE_SONG_FAILURE,
    payload: {
        error,
    }
});

export const DELETE_SONG_REQUEST = 'DELETE_SONG_REQUEST';
export const DELETE_SONG_SUCCESS = 'DELETE_SONG_SUCCESS';
export const DELETE_SONG_FAILURE = 'DELETE_SONG_FAILURE';

export const deleteSongRequest = (song) => ({
    type: DELETE_SONG_REQUEST,
    payload: {
        song,
    }
});
export const deleteSongSuccess = (songId, entities) => ({
    type: DELETE_SONG_SUCCESS,
    payload: {
        songId,
        entities,
    }
});
export const deleteSongFailure = (error) => ({
    type: DELETE_SONG_FAILURE,
    payload: {
        error,
    }
});