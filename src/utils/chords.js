const chordNumberObj = {
  0: {
    'fr': 'La',
    'en': 'A',
  },
  1: {
    'fr': 'Sib',
    'en': 'Bb',
  },
  2: {
    'fr': 'Si',
    'en': 'B',
  },
  3: {
    'fr': 'Do',
    'en': 'C',
  },
  4: {
    'fr': 'Do#',
    'en': 'C#',
  },
  5: {
    'fr': 'Ré',
    'en': 'D',
  },
  6: {
    'fr': 'Mib',
    'en': 'Eb',
  },
  7: {
    'fr': 'Mi',
    'en': 'E',
  },
  8: {
    'fr': 'Fa',
    'en': 'F',
  },
  9: {
    'fr': 'Fa#',
    'en': 'F',
  },
  10: {
    'fr': 'Sol',
    'en': 'G',
  },
  11: {
    'fr': 'Sol#',
    'en': 'G#',
  },
};


export const chordToNumber = (letter, alt) => {
  const chord = letter + (alt ? alt : '');
  // Search for all type of chord
  const nums = Object.keys(chordNumberObj);
  for (let i = 0; i < nums.length; i += 1) {
    const types = Object.keys(chordNumberObj[i]);
    for (let j = 0; j < types.length; j += 1) {
      if (chordNumberObj[i][types[j]] === chord) {
        return i;
      }
    }
  }
  return 0; // Default value
}
  
export const numberToChord = (val) => {
  return chordNumberObj[(val + 12) % 12]['fr'];
}