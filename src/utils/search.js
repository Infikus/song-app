export const isInString = (string, search) => {
  let formString = string.trim();
  let formSearch = search.trim();

  // Remove coma
  formString = formString.replace(',', '');
  formSearch = formSearch.replace(',', '');

  // Remove multiple whitespaces
  const spaceRegex = /[ ]+/g
  formString = formString.replace(spaceRegex, ' ');
  formSearch = formSearch.replace(spaceRegex, ' ');

  // Remove accents
  formString = formString.normalize('NFD').replace(/[\u0300-\u036f]/g, "")
  formSearch = formSearch.normalize('NFD').replace(/[\u0300-\u036f]/g, "")

  return formString.toLowerCase().includes(formSearch);
};
