import {
    call,
    put,
    takeEvery,
} from 'redux-saga/effects';
import * as actions from '../actions/teamMembers.action';
import { removeAuthToken, getJSONData, handleError401 } from './tools';
import { addTeamMemberAPI, updateTeamMemberAPI, deleteTeamMemberAPI } from './api/teamMembers.api';
import { teamMemberSchema } from './api/schemas';
import { normalize } from 'normalizr';

export function* addTeamMember(action) {
    try {
        const response = yield call(addTeamMemberAPI, action.payload.teamMember);
        yield put(actions.addTeamMemberSuccess(response.result, response.entities));
    } catch (e) {
        if (yield call(handleError401, e, action)) return;
        const error = getJSONData(e).error;
        yield put(actions.addTeamMemberFailure(error));
    }
}

export function* updateTeamMember(action) {
    try {
        const response = yield call(updateTeamMemberAPI, action.payload.teamMember);
        yield put(actions.updateTeamMemberSuccess(response.entities));
    } catch (e) {
        if (yield call(handleError401, e, action)) return;
        const error = getJSONData(e).error;
        yield put(actions.updateTeamMemberFailure(error));
    }
}

export function* deleteTeamMember(action) {
    try {
        const { teamMember } = action.payload;
        const response = yield call(deleteTeamMemberAPI, teamMember);
        yield put(actions.deleteTeamMemberSuccess(response.result, response.entities));
    } catch (e) {
        if (yield call(handleError401, e, action)) return;
        const error = getJSONData(e).error;
        yield put(actions.deleteTeamMemberFailure(error));
    }
}

export function* watcher() {
    yield [
        takeEvery(actions.ADD_TEAM_MEMBER_REQUEST, addTeamMember),
        takeEvery(actions.UPDATE_TEAM_MEMBER_REQUEST, updateTeamMember),
        takeEvery(actions.DELETE_TEAM_MEMBER_REQUEST, deleteTeamMember),
    ];
}

export default watcher;
