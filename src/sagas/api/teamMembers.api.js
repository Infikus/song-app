import fetch from 'cross-fetch';
import constants from './constants';

import { getAuthHeader, getAuthToken, getJSONData } from '../tools'
import { normalize } from 'normalizr';
import { teamMemberSchema } from './schemas';

const API_ENDPOINT = constants.endpoint;

export function addTeamMemberAPI(teamMember) {
  const teamMemberNoCircular = {
    ...teamMember,
    team: {
      ...teamMember.team,
      team_members: {}
    },
  };
  return fetch(API_ENDPOINT + '/team-members', {
    headers: getAuthHeader(),
    method: 'POST',
    body: JSON.stringify(teamMemberNoCircular),
  }).then (data => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    // Add the teamMember id in the user object
    const normData = normalize(jsonData, teamMemberSchema);
    const userId = Object.keys(normData.entities.users)[0];
    return {
      ...normData,
      entities: {
        ...normData.entities,
        users: {
          [userId]: {
            ...normData.entities.users[userId],
            team_members: [normData.result],
          },
        },
      }
    };
  })
}

export function updateTeamMemberAPI(teamMember) {
  const teamMemberNoCircular = {
    ...teamMember,
    user: {
      id: teamMember.user.id,
    },
    team: {
      id: teamMember.team.id,
    },
  };
  return fetch(API_ENDPOINT + '/team-members', {
    headers: getAuthHeader(),
    method: 'PUT',
    body: JSON.stringify(teamMemberNoCircular),
  }).then (data => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    return normalize(jsonData, teamMemberSchema);
  })
}

export function deleteTeamMemberAPI(teamMember) {
  const teamMemberNoCircular = {
    ...teamMember,
    user: {
      id: teamMember.user.id,
    },
    team: {
      id: teamMember.team.id,
    },
  };
  return fetch(API_ENDPOINT + '/team-members', {
    headers: getAuthHeader(),
    method: 'DELETE',
    body: JSON.stringify(teamMemberNoCircular),
  }).then (data => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = {
      ...getJSONData(data),
      id: teamMember.id
    };
    return normalize(jsonData, teamMemberSchema);
  })
}