import fetch from 'cross-fetch';
import constants from './constants';

import { getAuthHeader, getJSONData } from "../tools";
import { normalize } from 'normalizr';
import { songsSchema, songSchema } from './schemas';

const API_ENDPOINT = constants.endpoint;


export function postSongAPI(song, teamId) {
  const data = Object.assign({}, song, {
    team: { id: teamId }
  })
  return fetch(API_ENDPOINT + "/songs", {
    headers: getAuthHeader(),
    method: "POST",
    body: JSON.stringify(data)
  }).then(data => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    const normData = normalize(jsonData, songSchema);
    normData.entities.teams[teamId].songs = [normData.result];
    return normData;
  });
}

export function updateSongAPI(song) {
  // used to avoid circular structure in song object
  const songMod = {
    ...song,
    team: {
      id: song.team.id,
    },
    song_params: null,
  };
  console.log(songMod);
  return fetch(API_ENDPOINT + "/songs", {
    headers: getAuthHeader(),
    method: "PUT",
    body: JSON.stringify(songMod)
  }).then(data => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    return normalize(jsonData, songSchema);
  });
}

export function getSongAPI(songId) {
  let url = API_ENDPOINT + "/songs/" + songId;
  return fetch(url, {
    headers: getAuthHeader(),
    method: "GET",
  }).then(data => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    return normalize(jsonData, songSchema);
  });
};

export function getSongsAPI(teamId) {
  let url = API_ENDPOINT + '/songs?teamId=' + teamId;
  return fetch(url, {
    headers: getAuthHeader(),
    method: "GET",
  }).then(data => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    let normData = normalize(jsonData, songsSchema);
    const team = normData.entities.teams ? normData.entities.teams[teamId] : {};
    return {
      ...normData,
      entities: {
        ...normData.entities,
        teams: {
          [teamId]: {
            ...team,
            songs: normData.result,
          },
        },
      },
    };
  });
};

export function deleteSongAPI(song) {
  const dataNoCircular = {
    ...song,
    team: {
      id: song.team.id,
    },
    song_params: null,
  }
  let url = API_ENDPOINT + "/songs";
  return fetch(url, {
    headers: getAuthHeader(),
    method: "DELETE",
    body: JSON.stringify(dataNoCircular),
  }).then(data => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    return normalize(jsonData, songSchema);
  });
};