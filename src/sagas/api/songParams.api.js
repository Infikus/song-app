import fetch from 'cross-fetch';
import constants from './constants';
import { getAuthHeader, getJSONData } from "../tools";
import { normalize } from 'normalizr';
import { songParamsSchema } from './schemas';

const API_ENDPOINT = constants.endpoint + "/song-params";

export function setSongParamsAPI(songParams, song, songListItem) {
  const data = {
    ...songParams,
    song: song ? {
      id: song.id,
    } : null,
    song_list_item: songListItem ? {
      id: songListItem.id,
    } : null,
    user: songParams.user ? {
      id: songParams.user.id,
    } : null,
  };
  return fetch(API_ENDPOINT, {
    headers: getAuthHeader(),
    method: "POST",
    body: JSON.stringify(data)
  }).then(data => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    const normData = normalize(jsonData, songParamsSchema);
    return normData;
  });
}
