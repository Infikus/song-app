import { schema } from 'normalizr';

export const teamSchema = new schema.Entity('teams', { idAttribute: 'id' });
export const teamMemberSchema = new schema.Entity('team_members', { idAttribute: 'id' });
export const songSchema = new schema.Entity('songs', { idAttribute: 'id' });
export const songsSchema = [ songSchema ];
export const songListSchema = new schema.Entity('song_lists', { idAttribute: 'id' });
export const songListsSchema = [ songListSchema ];
export const songListItemSchema = new schema.Entity('song_list_items', { idAttribute: 'id' });
export const userSchema = new schema.Entity('users', { idAttribute: 'id' });
export const songParamsSchema = new schema.Entity('song_params', { idAttribute: 'id' });

teamSchema.define({
  team_members: [ teamMemberSchema ],
  songs: songsSchema,
  song_lists: songListsSchema,
});

teamMemberSchema.define({
  user: userSchema,
  team: teamSchema,
});

songSchema.define({
  team: teamSchema,
  song_params: [ songParamsSchema ],
});

songParamsSchema.define({
  user: userSchema,
  song: songSchema,
  song_list_item: songListItemSchema,
});

songListSchema.define({
  song_list_items: [ songListItemSchema ],
  team: teamSchema,
});

songListItemSchema.define({
  song: songSchema,
  song_list: songListSchema,
  song_params: [ songParamsSchema ],
});

userSchema.define({
  team_members: [ teamMemberSchema ],
  last_team_member: teamMemberSchema,
});


