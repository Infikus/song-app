import fetch from 'cross-fetch';
import constants from './constants';
import { getAuthHeader, getJSONData } from "../tools";
import { normalize } from 'normalizr';
import { songListsSchema, songListSchema } from './schemas';

const API_ENDPOINT = constants.endpoint + "/song-lists";


export function postSongListAPI(songList, teamId) {
  const data = Object.assign({}, songList, {
    team: { id: teamId }
  })
  console.log(songList);
  return fetch(API_ENDPOINT, {
    headers: getAuthHeader(),
    method: "POST",
    body: JSON.stringify(data)
  }).then(data => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    const normData = normalize(jsonData, songListSchema);
    // Add an empty array for song_list_items
    return {
      ...normData,
      entities: {
        ...normData.entities,
        song_lists: {
          [normData.result]: {
            ...normData.entities.song_lists[normData.result],
            song_list_items: [],
          },
        },
      },
    };
  });
}

export function updateSongListAPI(songList) {
  const dataWithoutCircularStruct = {
    ...songList,
    song_list_items: songList.song_list_items ? 
      songList.song_list_items.map(item => ({
        ...item,
        song: {
          ...item.song,
          team: null, // This object may cause a circular JSON object
        },
        song_params: null,
      })) :
      null,
    team: {
      id: songList.team.id,
    },
  };
  return fetch(API_ENDPOINT, {
    headers: getAuthHeader(),
    method: "PUT",
    body: JSON.stringify(dataWithoutCircularStruct)
  }).then(data => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    return normalize(jsonData, songListSchema);
  });
}

export function getSongListAPI(listId) {
  let url = API_ENDPOINT + "/" + listId;
  return fetch(url, {
    headers: getAuthHeader(),
    method: "GET",
  }).then(data => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    return normalize(jsonData, songListSchema);
  });
};

export function getSongListsAPI(teamId) {
  let url = API_ENDPOINT + "?teamId=" + teamId;
  return fetch(url, {
    headers: getAuthHeader(),
    method: "GET",
  }).then(data => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    const normData = normalize(jsonData, songListsSchema);
    const team = normData.entities.teams ? normData.entities.teams[teamId] : {};
    return {
      ...normData,
      entities: {
        ...normData.entities,
        teams: {
          [teamId]: {
            ...team,
            song_lists: normData.result,
          },
        },
      },
    };
  });
};

export function deleteSongListAPI(songList) {
  const dataWithoutCircularStruct = {
    ...songList,
    song_list_items: null,
    team: {
      id: songList.team.id,
    },
  };
  return fetch(API_ENDPOINT, {
    headers: getAuthHeader(),
    method: "DELETE",
    body: JSON.stringify(dataWithoutCircularStruct)
  }).then(data => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    return normalize(jsonData, songListSchema);
  });
}
