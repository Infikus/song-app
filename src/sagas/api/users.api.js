import fetch from 'cross-fetch';
import constants from './constants';

import { getAuthHeader, getAuthToken, getJSONData } from '../tools'
import { normalize } from 'normalizr';
import { userSchema, teamSchema } from './schemas';

const API_ENDPOINT = constants.endpoint;

export function meAPI() {
  return fetch(API_ENDPOINT + "/me", {
    headers: getAuthHeader(),
    method: "GET",
  }).then(data => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    return normalize(jsonData, userSchema);
  });
}

export function refreshTokenAPI(refreshToken) {
  return fetch(API_ENDPOINT + "/token/refresh", {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    method: "POST",
    body: JSON.stringify({
      refresh_token: refreshToken,
    }),
  }).then(data => {
    if (!data.ok) return Promise.reject(data);
    return JSON.parse(data._bodyInit);
  });
}

export function loginAPI(email, password) {
  return fetch(API_ENDPOINT + "/login_check", {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    method: "POST",
    body: JSON.stringify({
      email,
      password
    })
  }).then(data => {
    if (!data.ok) return Promise.reject(data);
    return JSON.parse(data._bodyInit);
  });
}

export function registerAPI(email, password) {
  return fetch(API_ENDPOINT + "/register", {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    method: "POST",
    body: JSON.stringify({
      email,
      password
    })
  }).then(data => {
    if (!data.ok) return Promise.reject(JSON.parse(data._bodyInit));
  });
}

export function newTeamAPI(team) {
  return fetch(API_ENDPOINT + "/teams", {
    headers: getAuthHeader(),
    method: "POST",
    body: JSON.stringify(team)
  }).then(data => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    const normData = normalize(jsonData, teamSchema);
    const teamMemberId = Object.keys(normData.entities.team_members)[0]; // Always only one teamMember
    // add the team id in the team_member object
    return {
      ...normData,
      entities: {
        ...normData.entities,
        team_members: {
          ...normData.entities.team_members,
          [teamMemberId]: {
            ...normData.entities.team_members[teamMemberId],
            team: normData.result,
          },
        },
      },
    };
  })
}

export function updateTeamAPI(team) {
  const teamNoCircular = {
    ...team,
    team_members: {},
  }
  return fetch(API_ENDPOINT + "/teams", {
    headers: getAuthHeader(),
    method: "PUT",
    body: JSON.stringify(teamNoCircular)
  }).then(data => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    const normData = normalize(jsonData, teamSchema);
    const nTeam_members = Object.keys(normData.entities.team_members).reduce((acc, id) => ({
      ...acc,
      [id]: {
        ...normData.entities.team_members[id],
        team: normData.result,
      }
    }), {});
    // add the team id in the team_member object
    return {
      ...normData,
      entities: {
        ...normData.entities,
        team_members: nTeam_members,
      },
    };
  })
}

export function getTeamAPI(teamId) {
  return fetch(API_ENDPOINT + "/teams/" + teamId, {
    headers: getAuthHeader(),
    method: "GET",
  }).then(data => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    const normData = normalize(jsonData, teamSchema);
    const modData = Object.keys(normData.entities.team_members).reduce((acc, id) => ({
      users: {
        ...acc.users,
        [normData.entities.team_members[id].user]: {
          ...normData.entities.users[normData.entities.team_members[id].user],
          team_members: [id],
        }
      },
      team_members: {
        ...acc.team_members,
        [id]: {
          ...normData.entities.team_members[id],
          team: normData.result,
        }
      }
    }), {});
    // add the team id in the team_member object
    return {
      ...normData,
      entities: {
        ...normData.entities,
        team_members: modData.team_members,
        songs: normData.entities.songs ? normData.entities.songs : [],
        users: {
          ...normData.entities.users,
          ...modData.users,
        }
      },
    };
  })
}

export function deleteTeamAPI(team) {
  return fetch(API_ENDPOINT + "/teams", {
    headers: getAuthHeader(),
    method: "DELETE",
    body: JSON.stringify({
      ...team,
      team_members: {},
    }),
  }).then(data => {
    console.log(data);
    if (!data.ok) return Promise.reject(data);
    console.log(team);
    const dataRes = {
      ...team,
      team_members: team.team_members.map(tm => ({
        ...tm,
        user: {
          id: tm.user.id,
        },
        team: {
          id: tm.team.id,
        },
      })),
    };
    console.log(dataRes);
    const normData = normalize(dataRes, teamSchema);
    console.log(normData);
    return normData;
  })
}