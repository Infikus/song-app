import fetch from 'cross-fetch';
import constants from './constants';
import { getAuthHeader, getJSONData } from "../tools";
import { normalize } from 'normalizr';
import { songListItemSchema } from './schemas';

const API_ENDPOINT = constants.endpoint + "/song-list-items";

export function updateSongListItemAPI(songListItem) {
  const dataWithoutCircularStruct = {
    id: songListItem.id,
    transpose: songListItem.transpose ? songListItem.transpose : 0,
  };
  return fetch(API_ENDPOINT, {
    headers: getAuthHeader(),
    method: "PUT",
    body: JSON.stringify(dataWithoutCircularStruct)
  }).then(data => {
    if (!data.ok) return Promise.reject(data);
    const jsonData = getJSONData(data);
    return normalize(jsonData, songListItemSchema);
  });
}