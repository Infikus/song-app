import {
  call,
  takeEvery,
  put,
  takeLatest,
  select,
} from 'redux-saga/effects';
import * as actions from '../actions/users.action';
import * as socketActions from '../actions/socket.actions';
import { loginAPI, registerAPI, meAPI, refreshTokenAPI } from './api/users.api';
import { push } from 'react-router-redux';
import { setAuthToken, removeAuthToken, onError, getRefreshToken, handleError401 } from './tools';
import { meSuccess } from '../actions/users.action';
import { getWaitingActions } from '../reducers/rootSelectors';

export function* me(socket, action) {
  try {
    const response = yield call(meAPI);
    yield put(actions.meSuccess(response.result, response.entities));
    yield call(socket.subscribe, response.result);
  } catch (e) {
    console.log(e);
    if (yield call(handleError401, e, action)) return;
    yield call(onError);
  }
}

export function* login(action) {
  try {
    const user = yield call(loginAPI, action.email, action.password);
    setAuthToken(user.token, user.refresh_token);
    yield put(actions.loginSuccess(user));
    // Redirect to the main page
    yield put(push('/'));
  } catch (e) {
    let msg = "Identifiants incorrects"
    if (e.status != 401 && e.code != 401) 
      msg = "Une erreur est survenue";

    yield put(actions.loginFailure(msg));
  }
}

export function* loginFailure(action) {
  // Currently, only delete the token if exists
  removeAuthToken();
}

export function* logout(action) {
  yield put(push('/login'));
  removeAuthToken();
}

export function* register(action) {
  try {
    const res = yield call(registerAPI, action.email, action.password);

    yield put(actions.registerSuccess());
  } catch (e) {
    yield put(actions.registerFailure(e.errors));
  }
}

export function* refreshToken(action) {
  try {
    const refreshToken = getRefreshToken();
    const data = yield call(refreshTokenAPI, refreshToken);
    setAuthToken(data.token);
    const waitingActions = yield select(getWaitingActions);
    yield put(actions.refreshTokenSuccess());
    for (let i = 0; i < waitingActions.length; i++) {
      yield put(waitingActions[i]);
    }
  } catch (e) {
    console.log(e);
    yield call(onError);
  }
}

export function* watcher(params) {
  yield [
    takeLatest(actions.LOGIN_REQUEST, login),
    takeLatest(actions.LOGIN_FAILURE, loginFailure),
    takeLatest(actions.REGISTER_REQUEST, register),
    takeLatest(actions.ME_REQUEST, me, params.socket),
    takeLatest(actions.REFRESH_TOKEN_REQUEST, refreshToken),
    takeLatest(actions.LOGOUT, logout),
  ];
}

export default watcher;
