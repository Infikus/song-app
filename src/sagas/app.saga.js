import {
    all,
    fork,
} from 'redux-saga/effects';
import songsSaga from './songs.saga';
import songListsSaga from './songLists.saga';
import usersSaga from './users.saga';
import teamsSaga from './teams.saga';
import teamMembersSaga from './teamMembers.saga';
import songParamsSaga from './songParams.saga';
import songListItemsSaga from './songListItems.saga';

export function* rootSaga(params) {
    yield all([
        fork(songsSaga),
        fork(songListsSaga),
        fork(usersSaga, params),
        fork(teamsSaga),
        fork(teamMembersSaga),
        fork(songParamsSaga),
        fork(songListItemsSaga),
    ])
}

export default rootSaga;
