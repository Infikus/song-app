import {
    call,
    takeEvery,
    put,
} from 'redux-saga/effects';
import * as actions from '../actions/songParams.action';
import { handleError401 } from './tools';
import { setSongParamsAPI } from './api/songParams.api';

export function* setSongParams(action) {
    try {
        const { song, songListItem, songParams } = action.payload;
        const response = yield call(setSongParamsAPI, songParams, song, songListItem);
        yield put(actions.setSongParamsSuccess(response.result, response.entities));
    } catch (e) {
        console.error(e);
        if (yield call(handleError401, e, action)) return;
        yield put(actions.setSongParamsFailure(e));
    }
}

export function* watcher() {
    yield [
        takeEvery(actions.SET_SONG_PARAMS_REQUEST, setSongParams),
    ]
}

export default watcher;

