import {
  call,
  takeEvery,
  put,
  select
} from 'redux-saga/effects';
import { push } from 'react-router-redux';
import * as actions from '../actions/songs.action';
import fetch from 'cross-fetch';
import { postSongAPI, getSongsAPI, getSongAPI, updateSongAPI, deleteSongAPI } from './api/songs.api';
import { handleError401 } from './tools';
import { getSelectedTeamId } from '../reducers/rootSelectors';

export function* getSong(action) {
  try {
    const { songId } = action.payload;
    const response = yield call(getSongAPI, songId);
    yield put(actions.getSongSuccess(response.result, response.entities));
  } catch (e) {
    if (yield call(handleError401, e, action)) return;
    yield put(actions.getSongFailure(e));
  }
}

export function* getSongs(action) {
  try {
    const teamId = yield select(getSelectedTeamId);
    const response = yield call(getSongsAPI, teamId);
    yield put(actions.getSongsSuccess(response.result, response.entities));
  } catch (e) {
    console.error(e);
    if (yield call(handleError401, e, action)) return;
    yield put(actions.getSongsFailure(e));
  }
}

export function* postSong(action) {
  try {
    const teamId = yield select(getSelectedTeamId);
    console.log(action);
    const { song } = action.payload;
    const response = yield call(postSongAPI, song, teamId)
    yield put(actions.postSongSuccess(response.result, response.entities));
  } catch (e) {
    console.error(e);
    if (yield call(handleError401, e, action)) return;
    yield put(actions.postSongFailure(e));
  }
}

export function* updateSong(action) {
  try {
    const response = yield call(updateSongAPI, action.payload.song)
    yield put(actions.updateSongSuccess(response.entities));
  } catch (e) {
    console.log(e);
    if (yield call(handleError401, e, action)) return;
    yield put(actions.updateSongFailure(e));
  }
}

export function* deleteSong(action) {
  try {
    const response = yield call(deleteSongAPI, action.payload.song)
    yield put(actions.deleteSongSuccess(action.payload.song.id, response.entities));
    // Redirect to the list of songs
    yield put(push('/songs'));
  } catch (e) {
    console.log(e);
    if (yield call(handleError401, e, action)) return;
    yield put(actions.deleteSongFailure(e));
  }
}

export function* watcher() {
  yield [
    takeEvery(actions.POST_SONG_REQUEST, postSong),
    takeEvery(actions.UPDATE_SONG_REQUEST, updateSong),
    takeEvery(actions.FETCH_SONGS_REQUEST, getSongs),
    takeEvery(actions.FETCH_SONG_REQUEST, getSong),
    takeEvery(actions.DELETE_SONG_REQUEST, deleteSong),
  ]
}

export default watcher;
