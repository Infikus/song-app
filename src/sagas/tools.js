import { push } from 'react-router-redux';
import { put, select } from 'redux-saga/effects';
import { loginFailure, refreshTokenRequest, addWaitingAction } from '../actions/users.action';
import jwt from 'jsonwebtoken';
import { isTokenRefreshing } from '../reducers/rootSelectors';

export function setAuthToken(token, refreshToken=null) {
    localStorage.setItem('JWT', token);
    if (refreshToken) localStorage.setItem('JWT_refresh', refreshToken);
};

export function getAuthToken() {
    return localStorage.getItem('JWT');
};

export function getRefreshToken() {
    return localStorage.getItem('JWT_refresh');
}

export function removeAuthToken() {
    localStorage.removeItem('JWT');
    localStorage.removeItem('JWT_refresh');
};

export function isTokenValid() {
    const token = getAuthToken();
    if (!token) return false;
    try {
        const user = jwt.decode(token);
        return user.exp > new Date().getTime() / 1000;
    } catch (e) {
        return false;
    }
};

export function getAuthHeader() {
    return {
        'Authorization': 'Bearer ' + getAuthToken(),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    };
};

export function setSelectedTeam(teamId) {
    localStorage.setItem('selectedTeam', teamId);
}

export function* onError() {
    yield put(loginFailure("Session expirée"));
    yield put(push('/login'));
}

export function* handleError401(error, action) {
    if (error.status == 401 || error.code == 401) {
        const isRefreshing = yield select(isTokenRefreshing);
        if (!isRefreshing) {
            yield put(refreshTokenRequest());
        }
        yield put(addWaitingAction(action));
        return true;
    }
    return false;
}

export function getJSONData(requestResponse) {
    return JSON.parse(requestResponse._bodyInit);
}