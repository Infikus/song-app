import {
  call,
  takeEvery,
  put,
} from 'redux-saga/effects';
import * as actions from '../actions/songListItems.action';
import { handleError401 } from './tools';
import { updateSongListItemAPI } from './api/songListItems.api';

export function* updateSongListItem(action) {
  try {
    const { songListItem } = action.payload;
    const response = yield call(updateSongListItemAPI, songListItem)
    yield put(actions.updateSongListItemSuccess(response.result, response.entities));
  } catch (e) {
    console.error(e);
    if (yield call(handleError401, e, action)) return;
    yield put(actions.updateSongListItemFailure(e));
  }
}

export function* watcher() {
  yield [
    takeEvery(actions.UPDATE_SONG_LIST_ITEM_REQUEST, updateSongListItem),
  ];
}

export default watcher;
