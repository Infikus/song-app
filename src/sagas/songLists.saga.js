import {
  call,
  takeEvery,
  put,
  select
} from 'redux-saga/effects';
import { push } from 'react-router-redux';
import * as actions from '../actions/songLists.action';
import { postSongListAPI, getSongListsAPI, getSongListAPI, updateSongListAPI, deleteSongListAPI } from './api/songLists.api';
import { handleError401 } from './tools';
import { getSelectedTeamId } from '../reducers/rootSelectors';

export function* getSongList(action) {
  try {
    const response = yield call(getSongListAPI, action.payload.songListId);
    yield put(actions.getSongListSuccess(response.entities));
  } catch (e) {
    console.error(e);
    if (yield call(handleError401, e, action)) return;
    yield put(actions.getSongListFailure(e));
  }
}

export function* getSongLists(action) {
  try {
    const teamId = yield select(getSelectedTeamId);
    const response = yield call(getSongListsAPI, teamId);
    yield put(actions.getSongListsSuccess(response.entities, response.result));
  } catch (e) {
    console.error(e);
    if (yield call(handleError401, e, action)) return;
    yield put(actions.getSongListsFailure(e));
  }
}

export function* postSongList(action) {
  try {
    const teamId = yield select(getSelectedTeamId);
    const response = yield call(postSongListAPI, action.payload.songList, teamId);
    yield put(actions.postSongListSuccess(response.result, response.entities));
    yield put(push('/list/'+response.result))
  } catch (e) {
    console.error(e);
    if (yield call(handleError401, e, action)) return;
    yield put(actions.postSongListFailure(e));
  }
}

export function* updateSongList(action) {
  try {
    const { songList } = action.payload;
    const response = yield call(updateSongListAPI, songList)
    yield put(actions.updateSongListSuccess(response.entities));
  } catch (e) {
    console.error(e);
    if (yield call(handleError401, e, action)) return;
    yield put(actions.updateSongListFailure(e));
  }
}

export function* deleteSongList(action) {
  try {
    const response = yield call(deleteSongListAPI, action.payload.songList)
    yield put(actions.deleteSongListSuccess(action.payload.songList.id, response.entities));
    yield put(push('/lists'));
  } catch (e) {
    console.error(e);
    if (yield call(handleError401, e, action)) return;
    yield put(actions.deleteSongListFailure(e));
  }
}

export function* watcher() {
  yield [
    takeEvery(actions.POST_SONG_LIST_REQUEST, postSongList),
    takeEvery(actions.UPDATE_SONG_LIST_REQUEST, updateSongList),
    takeEvery(actions.FETCH_SONG_LISTS_REQUEST, getSongLists),
    takeEvery(actions.FETCH_SONG_LIST_REQUEST, getSongList),
    takeEvery(actions.DELETE_SONG_LIST_REQUEST, deleteSongList),
  ]
}

export default watcher;
