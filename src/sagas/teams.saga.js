import {
    call,
    takeEvery,
    put,
    takeLatest
} from 'redux-saga/effects';
import { push } from 'react-router-redux';
import * as actions from '../actions/teams.action';
import { newTeamAPI, updateTeamAPI, deleteTeamAPI, getTeamAPI } from './api/users.api';
import { setSelectedTeam as setSelectedTeamLS, getJSONData, handleError401 } from './tools';

export function* newTeam(action) {
    try {
        const response = yield call(newTeamAPI, action.payload.team);
        yield put(actions.postTeamSuccess(response.result, response.entities));
        yield put(push('/settings/team/'+response.result));
    } catch (e) {
        if (yield call(handleError401, e, action)) return;
        const error = getJSONData(e).error;
        yield put(actions.postTeamFailure(error));
    }
}

export function* updateTeam(action) {
    try {
        const response = yield call(updateTeamAPI, action.payload.team);
        yield put(actions.updateTeamSuccess(response.entities));
    } catch (e) {
        if (yield call(handleError401, e, action)) return;
        const error = getJSONData(e).error;
        yield put(actions.updateTeamFailure(error));
    }
}

export function* deleteTeam(action) {
    try {
        const response = yield call(deleteTeamAPI, action.payload.team);
        yield put(actions.deleteTeamSuccess(response.result, response.entities));
        yield put(push('/settings'));
    } catch (e) {
        if (yield call(handleError401, e, action)) return;
        const error = getJSONData(e).error;
        yield put(actions.deleteTeamFailure(error));
    }
}

export function* getTeam(action) {
    try {
        const response = yield call(getTeamAPI, action.payload.teamId);
        yield put(actions.fetchTeamSuccess(response.result, response.entities));
    } catch (e) {
        if (yield call(handleError401, e, action)) return;
        console.log(e);
        yield put(actions.fetchTeamFailure("Une erreur est survenue"));
    }
}

export function* setSelectedTeam(action) {
    setSelectedTeamLS(action.payload.teamId);
}

export function* watcher() {
    yield [
        takeLatest(actions.POST_TEAM_REQUEST, newTeam),
        takeLatest(actions.UPDATE_TEAM_REQUEST, updateTeam),
        takeLatest(actions.DELETE_TEAM_REQUEST, deleteTeam),
        takeLatest(actions.SET_SELECTED_TEAM, setSelectedTeam),
        takeEvery(actions.FETCH_TEAM_REQUEST, getTeam),
    ];
}

export default watcher;
