import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './reducers/app.reducer';
import rootSaga from './sagas/app.saga';
import configureSocket from './socket';

// Styles
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css';
// Import Main styles for this application
import '../scss/style.scss'
// Temp fix for reactstrap
import '../scss/core/_dropdown-menu-right.scss'

// Containers
import Full from './containers/Full/'

import createHistory from 'history/createBrowserHistory'
import { Route, Switch } from 'react-router'
import { PrivateRoute } from './routes';

import { ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux'
import LoginView from './views/Login.view';
import authMiddleware from './middlewares/auth.middleware';


// Create a history of your choosing (we're using a browser history in this case)
const history = createHistory()

// Build the middleware for intercepting and dispatching navigation actions
const middleware = routerMiddleware(history)

const sagaMiddleware = createSagaMiddleware();

// Add the reducer to your store on the `router` key
// Also apply our middleware for navigating
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(authMiddleware, sagaMiddleware, middleware))
);

const socket = configureSocket(store.dispatch);

sagaMiddleware.run(rootSaga, { socket });

ReactDOM.render((
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Switch>
        <Route path="/login" name="Login" component={LoginView}/>
        <PrivateRoute path="/" name="Home" component={Full}/>
      </Switch>
    </ConnectedRouter>
  </Provider>
), document.getElementById('root'));