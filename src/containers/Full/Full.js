import React, {Component} from 'react';
import { connect } from 'react-redux';
import {Link, Switch, Route, Redirect} from 'react-router-dom';
import { PrivateRoute } from '../../routes';
import {Container} from 'reactstrap';
import Header from '../../components/Header/';
import Sidebar from '../../components/Sidebar/';
import Breadcrumb from '../../components/Breadcrumb/';
import Aside from '../../components/Aside/';
import Footer from '../../components/Footer/';

import Dashboard from '../../views/Dashboard/';
import Songs from '../../views/Songs.view';
import ShowSong from '../../views/ShowSong.view';
import SongLists from '../../views/SongLists.view';
import ShowSongList from '../../views/ShowSongList.view';
import TeamSettings from '../../views/TeamSettings.view';
import Settings from '../../views/Settings.view';
import { logout } from '../../actions/users.action';

class Full extends Component {
  render() {
    return (
      <div className="app">
        <Header
          logout={this.props.logout}
        />
        <div className="app-body">
          <Sidebar {...this.props}/>
          <main className="main">
            <Breadcrumb />
            <Container fluid>
              <Switch>
                <PrivateRoute path="/songs" name="Songs" component={Songs}/>
                <PrivateRoute path="/song/:songId" name="ShowSong" component={ShowSong}/>
                <PrivateRoute path="/settings/team/:teamId" name="teamSettings" component={TeamSettings}/>
                <PrivateRoute path="/settings" name="settings" component={Settings}/>
                <PrivateRoute path="/lists" name="Lists" component={SongLists}/>
                <PrivateRoute path="/list/:songListId" name="ShowSongList" component={ShowSongList}/>
                <Redirect from="/" to="/songs"/>
              </Switch>
            </Container>
          </main>
          <Aside />
        </div>
        <Footer />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logout()),
})

export default connect(null, mapDispatchToProps)(Full);
