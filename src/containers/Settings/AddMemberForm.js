import React, {Component} from 'react';
import AddMemberForm from '../../forms/addMember.form';
import { connect } from 'react-redux';
import { addTeamMemberRequest } from '../../actions/teamMembers.action';
import { isPending } from '../../reducers/rootSelectors';

class AddMemberFormContainer extends Component {
    constructor(props) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(payload, { setSubmitting }) {
        const teamMember = {
            email: payload.email,
            role: payload.role,
            user: {
                email: payload.email
            },
            team: this.props.team,
        };

        this.props.addTeamMemberRequest(teamMember);
        setSubmitting(false);
    }

    render() {
        return (
            <AddMemberForm 
                onSubmit={this.onSubmit} 
                initialValues={{
                    role: 1,
                    email: "",
                }} 
                {...this.props} 
            />
        )
    }
}

const mapStateToProps = (state) => ({
    pending: isPending("ADD_TEAM_MEMBER", state),
})

const mapDispatchToProps = (dispatch, ownProps) =>  ({
    addTeamMemberRequest: (teamMember) => dispatch(addTeamMemberRequest(teamMember)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddMemberFormContainer);