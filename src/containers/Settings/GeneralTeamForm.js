import React, {Component} from 'react';
import GeneralTeamForm from '../../forms/generalTeam.form';
import { connect } from 'react-redux';
import { updateTeamRequest, deleteTeamRequest } from '../../actions/teams.action';
import { isPending, isSuccessful, isError } from '../../reducers/rootSelectors';
import ErrorMessage from '../../components/Common/ErrorMessage';

class GeneralTeamFormContainer extends Component {

  render() {
    return (
      <div>
        <ErrorMessage show={this.props.saveSuccess} timeout={3} color={"success"}>Modifications enregistrées</ErrorMessage>
        <ErrorMessage show={!!this.props.saveError}>{this.props.saveError}</ErrorMessage>
        <ErrorMessage show={!!this.props.deleteError}>{this.props.deleteError}</ErrorMessage>
        <GeneralTeamForm 
          onSubmit={this.props.onSubmit} 
          initialValues={this.props.team} 
          canEdit={this.props.canEdit}
          onDeleteClick={() => this.props.onDelete(this.props.team)}
          enableReinitialize={true}
          savePending={this.props.savePending}
          deletePending={this.props.deletePending}
          {...this.props} 
        />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  savePending: isPending("UPDATE_TEAM", state),
  saveError: isError("UPDATE_TEAM", state),
  saveSuccess: isSuccessful("UPDATE_TEAM", state),
  deletePending: isPending("DELETE_TEAM", state),
  deleteError: isError("DELETE_TEAM", state),
})

const mapDispatchToProps = (dispatch) =>  ({
  onSubmit: (payload, { setSubmitting }) => {
    dispatch(updateTeamRequest(payload));
    setSubmitting(false);
  },
  onDelete: (team) => {
    dispatch(deleteTeamRequest(team));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(GeneralTeamFormContainer);