import React from 'react';
import { connect } from 'react-redux';
import { updateSong } from '../../actions/songs.action';
import SongForm from '../../forms/song.form';
import { isPending } from '../../reducers/rootSelectors';

const UpdateSongFormContainer = ({
    afterSubmit,
    ...props
}) => (
    <SongForm afterSubmit={afterSubmit} {...props} />
);

const mapDispatchToProps = (dispatch) => ({
    onSubmit: (payload, { setSubmitting }) => {
        dispatch(updateSong(payload));
        setSubmitting(false);
    },
});

const mapStateToProps = (state) => ({
    pending: isPending("UPDATE_SONG", state),
})

const UpdateSongForm = connect(mapStateToProps, mapDispatchToProps)(UpdateSongFormContainer);

export default UpdateSongForm;

