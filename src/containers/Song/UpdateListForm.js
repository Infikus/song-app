import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateSongList } from '../../actions/songLists.action';
import NewListForm from '../../forms/newList.form';
import moment from 'moment';
import { isPending } from '../../reducers/rootSelectors';

class UpdateListFormContainer extends Component {

  componentWillReceiveProps(nextProps) {
    if (!nextProps.pending && this.props.pending) {
      nextProps.onSave();
    }
  }

  render() {
    const { afterSubmit, ...props } = this.props;
    return <NewListForm afterSubmit={afterSubmit} {...props} />;
  }
}

const mapDispatchToProps = (dispatch) => ({
  onSubmit: (payload, { setSubmitting }) => {
    dispatch(updateSongList(payload));
    setSubmitting(false);
  },
});

const mapStateToProps = (state) => ({
  pending: isPending("UPDATE_SONG_LIST", state),
})
const UpdateListForm = connect(mapStateToProps, mapDispatchToProps)(UpdateListFormContainer);

export default UpdateListForm;