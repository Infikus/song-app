import React from 'react';
import { connect } from 'react-redux';
import { postSongList } from '../../actions/songLists.action';
import NewListForm from '../../forms/newList.form';
import moment from 'moment';
import { isPending } from '../../reducers/rootSelectors';

const CreateListFormContainer = ({
    afterSubmit,
    ...props
}) => (
    <NewListForm afterSubmit={afterSubmit} {...props} />
);

const mapDispatchToProps = (dispatch) => ({
    onSubmit: (payload, { setSubmitting }) => {
        dispatch(postSongList(payload));
        setSubmitting(false);
    },
});

const mapStateToProps = (state) => ({
    pending: isPending("POST_SONG_LIST", state),
    initialValues: {
        date: moment().toDate(),
    }
})
const CreateListForm = connect(mapStateToProps, mapDispatchToProps)(CreateListFormContainer);

export default CreateListForm;

