import React from 'react';
import { connect } from 'react-redux';
import { postSong } from '../../actions/songs.action';
import SongForm from '../../forms/song.form';
import { isPending } from '../../reducers/rootSelectors';

const CreateSongFormContainer = ({
    afterSubmit,
    ...props
}) => (
    <SongForm afterSubmit={afterSubmit} {...props} />
);

const mapDispatchToProps = (dispatch) => ({
    onSubmit: (payload, { setSubmitting }) => {
        dispatch(postSong(payload));
        setSubmitting(false);
    },
});

const mapStateToProps = (state) => ({
    pending: isPending("POST_SONG", state),
    initialValues: {
        key: "0",
    }
})
const CreateSongForm = connect(mapStateToProps, mapDispatchToProps)(CreateSongFormContainer);

export default CreateSongForm;

