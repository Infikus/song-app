import React from 'react';
import { connect } from 'react-redux';
import { postList } from '../../actions/songLists.action';
import moment from 'moment';
import NewTeamForm from '../../forms/newTeam.form';
import { postTeamRequest } from '../../actions/teams.action';
import { isPending } from '../../reducers/app/me.reducer';

const NewTeamFormContainer = ({
    afterSubmit,
    ...props
}) => (
    <NewTeamForm 
        afterSubmit={afterSubmit} 
        initialValues={{
            name: "",
        }}
        {...props} />
);

const mapDispatchToProps = (dispatch) => ({
    onSubmit: (payload, { setSubmitting }) => {
        dispatch(postTeamRequest(payload));
        setSubmitting(false);
    },
});

const mapStateToProps = (state) => ({
    pending: isPending("POST_TEAM", state),
})

export default connect(null, mapDispatchToProps)(NewTeamFormContainer);

