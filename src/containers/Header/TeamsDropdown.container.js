import React, { Component } from 'react';
import { connect } from 'react-redux';
import TeamsDropdown from '../../components/Header/TeamsDropdown';
import { meRequest } from '../../actions/users.action';
import { getMyTeams, getSelectedTeamId } from '../../reducers/rootSelectors';
import FormModal from '../../components/FormModal/FormModal';
import NewTeamForm from './NewTeamForm';
import { setSelectedTeam } from '../../actions/teams.action';

class TeamsDropdownContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newTeamModal: false,
    }

    this.toggleNewTeamModal = this.toggleNewTeamModal.bind(this);
  }

  toggleNewTeamModal() {
    this.setState({
      newTeamModal: !this.state.newTeamModal,
    });
  }

  render() {
    const { teams, selectedTeamId, setSelectedTeam } = this.props;
    return (
      <div>
        <TeamsDropdown 
          teams={teams} 
          selectedTeamId={selectedTeamId}
          setSelectedTeam={setSelectedTeam}
          onNewTeamClick={this.toggleNewTeamModal}
        /> 
        <FormModal
          className="vertical-align-center"
          Form={NewTeamForm}
          title="Nouveau groupe"
          modalSize="md"
          isOpen={this.state.newTeamModal}
          toggle={this.toggleNewTeamModal}
          afterSubmit={ () => { this.toggleNewTeamModal(); }}
        />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  teams: getMyTeams(state),
  selectedTeamId: getSelectedTeamId(state),
})

const mapDispatchToProps = (dispatch) => ({
  setSelectedTeam: (teamId) => {
    dispatch(setSelectedTeam(teamId));
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(TeamsDropdownContainer);