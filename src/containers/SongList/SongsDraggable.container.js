import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Droppable, Draggable } from 'react-beautiful-dnd';
import { Card, CardBody, CardTitle, ListGroup, ListGroupItem } from 'reactstrap';

import SongItem from '../../components/ShowList/SongItem';
import scssStyle from '../../components/ShowList/ShowList.scss';

import { FILTER_TITLE_ASC } from '../../reducers/app/me.reducer';
import { getSelectedTeamSongsPaginate, getSelectedTeamSongs } from '../../reducers/rootSelectors';
import SongListPagination from '../../components/SongList/SongListPagination';
import SongListFilters from '../../components/SongList/SongListFilters';

class SongsDraggable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      pageSize: 15,
      search: props.search,
      filter: FILTER_TITLE_ASC,
    }
    this.state.songs = props.getSongs(this.state.page, this.state.pageSize, this.state.search, this.state.filter);

    this.onPageChange = this.onPageChange.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
    this.onFilterChange = this.onFilterChange.bind(this);
    this.checkPage = this.checkPage.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      songs: nextProps.getSongs(this.state.page, this.state.pageSize, this.state.search, this.state.filter),
    });
  }

  checkPage() {
    const { page, pageSize, songs } = this.state;
    const maxPage = Math.floor(songs.count/pageSize);
    if (page !== 0 && page > maxPage) {
      this.onPageChange(maxPage);
    }
  }

  onPageChange(page) {
    this.setState(prevState => ({
      songs: this.props.getSongs(page, prevState.pageSize, prevState.search, prevState.filter),
      page,
    }), () => this.checkPage());

  }

  onSearchChange(search) {
    this.props.onSearchChange(search);
    this.setState(prevState => ({
      songs: this.props.getSongs(prevState.page, prevState.pageSize, search, prevState.filter),
      search,
    }), () => this.checkPage());
  }

  onFilterChange(filter) {
    this.setState(prevState => ({
      songs: this.props.getSongs(prevState.page, prevState.pageSize, prevState.search, filter),
      filter,
    }));
  }

  render() {
    const { id, style } = this.props;
    const { songs, filter, search, page, pageSize } = this.state;
    return (
      <Card style={style} id="songsDraggable">
        <CardBody>
          <CardTitle>Vos chants</CardTitle>
        </CardBody>
        <div id="songsFilters">
          <SongListFilters
            onFilterChange={this.onFilterChange}
            filter={filter}
            onSearchChange={this.onSearchChange}
            search={search}
            showFilter
          />
        </div>
        <ListGroup className="listGroup">
          <Droppable droppableId={id} isDropDisabled={true}>
            {(provided, snapshot) => (
              <div ref={provided.innerRef}>
                { songs.data.map(song => (
                  <Draggable key={song.id} draggableId={"draggable"+song.id}>
                    {(provided, snapshot) => (
                      <div>
                        <div className="draggable">
                          <div ref={provided.innerRef} {...provided.dragHandleProps} style={provided.draggableStyle}>
                            <div className="songItem">
                              <SongItem 
                                song={song}
                              />
                            </div>
                          </div>
                          { snapshot.isDragging ?
                              <SongItem song={song} style={{
                                position: 'absolute', top: '10px', width: '100%'
                              }} /> 
                            :
                            null }
                        </div>
                        { provided.placeholder }
                      </div>
                    )}
                  </Draggable>
                )) }
                { provided.placeholder}
              </div>
            )}
          </Droppable>
        </ListGroup>
        <SongListPagination
          pageCount={Math.ceil(this.state.songs.count/pageSize)}
          page={page}
          onPageChange={this.onPageChange}
          pageRangeDisplayed={3}
          marginPagesDisplayed={1}
          nextLabel={">"}
          previousLabel={"<"}
        />
      </Card>
    )
  }
}

const mapStateToProps = (state) => ({
  getSongs: (page, pageSize, search, filter) => getSelectedTeamSongsPaginate(page, pageSize, search, filter, state),
  songs: getSelectedTeamSongs(state),
});

const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(SongsDraggable);
