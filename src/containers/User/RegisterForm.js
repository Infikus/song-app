import React from 'react';
import { connect } from 'react-redux';
import { registerRequest } from '../../actions/users.action';
import RegisterForm from '../../forms/register.form';
import { REGISTER_SUCCESS } from '../../actions/users.action';
import { isPending, isError } from '../../reducers/rootSelectors';
import ErrorMessage from '../../components/Common/ErrorMessage';

const RegisterFormContainer = ({
  afterSubmit,
  error,
  ...props
}) => (
  <div>
    <ErrorMessage show={!!error}>{error}</ErrorMessage>
    <RegisterForm afterSubmit={afterSubmit} {...props} />
  </div>
);

const mapDispatchToProps = (dispatch) => ({
  onSubmit: (payload, { setSubmitting }) => {
    dispatch(registerRequest(payload));
    setSubmitting(false);
  },
});

const mapStateToProps = (state) => ({
  pending: isPending("REGISTER", state),
  error: isError("REGISTER", state),
})

export default connect(mapStateToProps, mapDispatchToProps)(RegisterFormContainer);
