import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Redirect} from 'react-router';
import { isAuthenticated } from './sagas/tools';
import { meRequest } from './actions/users.action';
import { isMePending, isPending } from './reducers/rootSelectors';
import Loading from './components/Loading/Loading';

const routes = {
  '/': 'Home',
  '/dashboard': 'Dashboard',
  '/songs' : 'Liste des chants',
  '/lists' : 'Listes',
  '/settings': 'Paramètres'
};
export default routes;

class PrivateRouteComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pending: true,
      request: false,
    }
  }

  componentDidMount() {
    this.props.loadMe();
    this.setState({
      request: true,
    })
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.pending && this.state.request && this.state.pending) {
      this.setState({
        pending: false,
      });
    }
  }

  render() {
    const {
      component,
      pending,
      loadMe,
      ...rest
    } = this.props;
    const Children = component;
    if (this.state.pending) {
      return (<div style={{height: "100%"}}><Loading /></div>);
    } else {
      return (
        <Route {...rest} render={props => (
          true ? (
            <Children {...props}/>
          ) : (
            <Redirect to={{
              pathname: '/login',
              state: { from: props.location }
            }}/>
          )
        )}/>
      )
    }
  }
}

const mapStateToProps = (state) => ({
  pending: isPending("ME", state),
});

const mapDispatchToProps = (dispatch) => ({
  loadMe: () => {
    dispatch(meRequest());
  },
});

export const PrivateRoute = connect(mapStateToProps, mapDispatchToProps)(PrivateRouteComponent);