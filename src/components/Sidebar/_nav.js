export default {
  items: [
    {
      name: 'Chants',
      url: '/songs',
      icon: 'fa fa-music'
    },
    {
      name: 'Listes',
      url: '/lists',
      icon: 'fa fa-list'
    },
    {
      name: 'Paramètres',
      url: '/settings',
      icon: 'fa fa-gear'
    }
  ]
};
