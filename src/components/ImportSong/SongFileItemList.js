import React, { Component } from 'react';
import { ListGroup } from 'reactstrap';

import Pagination from '../SongList/SongListPagination';
import SongFileItem from './SongFileItem';

import paginationStyle from '../SongList/SongList.scss';

class SongFileItemList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 0,
      pageSize: 5,
    };

    this.onPageChange = this.onPageChange.bind(this);
  }

  onPageChange(page) {
    this.setState({
      page,
    });
  }

  getFiles() {
    return this.props.files.filter((f, id) => Math.floor(id/this.state.pageSize) === this.state.page);
  }

  render() {
    const { files } = this.props;
    return (
      <div>
        <ListGroup style={{ marginBottom: '1em' }}>
          { this.getFiles().map(file => (
            <SongFileItem
              key={file.id}
              fileName={file.name}
              isFileValid={!!file.song}
              pending={file.pending}
              success={file.success}
              error={file.error}
              warnings={file.warnings}
            />
          ))}
        </ListGroup>
        { files.length > this.state.pageSize && (
            <Pagination
              pageCount={Math.ceil(files.length/this.state.pageSize)}
              onPageChange={this.onPageChange}
              page={this.state.page}
            />
          )
        }
      </div>
    );
  }
}

export default SongFileItemList;