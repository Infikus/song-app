import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ListGroup } from 'reactstrap';
import Dropzone from 'react-dropzone';
import uuid from 'uuid/v4';
import SongContent from '../Song/SongContent';
import SongFileItem from './SongFileItem';
import SongFileItemList from './SongFileItemList';

class ImportSong extends Component {
  /* output if no errors :
   * {
   *   song: [Song],
   *   warnings: array(String),
   * }
   * output if there is an error :
   * {
   *   error: [string],
   * }
   */
  static parseSong(str) {
    const titleRegex= /\{(t|title):([^\}\{\n\t]*)\}\n?/g;
    const subtitleRegex= /\{(st|subtitle):([^\}\{\n\t]*)\}\n?/g;
    const keyRegex= /\{(key):([^\}\{\n\t]*)\}\n?/g;
    const chordRegex = /(A|B|C|D|E|F|G)(#|b)?(.*)/;
    const tempoRegex= /\{(tempo):([^\}\{\n\t]*)\}\n?/g;


    let song = {
      title: "",
      author: null,
      bpm: null,
      key: 0,
    };
    let content = str;
    let warnings = [];

    // Get the title and check if there is more than one title
    let hasTitle = false;
    let titleError = false;
    content = content.replace(titleRegex, (cor, type, value) => {
      if (hasTitle && !titleError) {
        warnings.push("Plusieurs titres dans le fichier. Le premier a été choisi et les autres ignorés.");
        titleError = true;
      } else if (!hasTitle) {
        hasTitle = true;
        song.title = value;
      }
      return '';
    })
    if (!hasTitle) return { error: "Pas de titre trouvé." };

    // Get the subtitle
    content = content.replace(subtitleRegex, (cor, type, value) => {
      // Remove subtitles
      return '';
    })

    // Get the key and check if there is more than one key
    let hasKey = false;
    let keyError = false;
    content = content.replace(keyRegex, (cor, type, keyString) => {
      if (hasKey && !keyError) {
        warnings.push("Plusieurs tonalités dans le fichier. La première a été choisie et les autres ignorées.");
        keyError = true;
      } else if (!hasKey) {
        hasKey = true;
        if (!keyString.match(chordRegex)) return { error: "Tonalité invalide." };
        song.key = keyString.replace(chordRegex, (c, letter, alt) => SongContent.chordToNumber(letter, alt));
      }
      return '';
    })

    if (!hasKey) warnings.push("Pas de tonalité trouvé. Tonalité par défaut: Do.");

    song.content = content;

    return {
      song,
      warnings,
    }
  }
  
  constructor(props) {
    super(props);
    this.state = {
      files: [],
      currentSaveId: undefined,
    }
    this.onDrop = this.onDrop.bind(this);
    this.updateFile = this.updateFile.bind(this);
    this.saveSong = this.saveSong.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    // There is no song that is saving
    if (!nextProps.pending) {
      // Search for a new song to save
      const file = this.getNextSongToSave();
      if (file === null) {
        if (this.state.currentSaveId !== undefined) {
          this.setState({
            currentSaveId: undefined,
          })
        }
      } else {
        // Save the next song
        this.saveSong(file.id, file.song)
      }

      if (this.state.currentSaveId !== undefined) {
        const index = this.state.files.findIndex(f => f.id === this.state.currentSaveId);
        if (index >= 0) {
          const nFile = {
            ...this.state.files[index],
            pending: false,
            success: nextProps.success,
            error: nextProps.error,
          };
          this.updateFile(nFile);
        }
      }
    }
  }

  getNextSongToSave() {
    const currentSaveId = this.state.currentSaveId;
    for (let i = 0; i < this.state.files.length; i += 1) {
      if (this.state.files[i].pending && currentSaveId !== this.state.files[i].id) return this.state.files[i];
    }

    return null;
  }

  addFile(file) {
    this.setState(prevState => ({
      files: [...prevState.files, file],
    }));
  }

  updateFile(file) {
    this.setState(prevState => {
      const index = prevState.files.findIndex(f => file.id === f.id);
      if (index >= 0) {
        return {
          files: [...prevState.files.slice(0, index), file, ...prevState.files.slice(index+1)],
        }
      }
      return {}; 
    });
  }

  saveSong(id, song) {
    this.setState({
      currentSaveId: id,
    }, () => this.props.saveSong(song));
  }

  onDrop(files) {
    for (let i = 0; i < files.length; i += 1) {
      const file = {
        id: uuid(),
        name: files[i].name,
        pending: true,
      };
      this.addFile(file);

      const reader = new FileReader();
      reader.onload = () => {
          const fileContent = reader.result;
          const response = ImportSong.parseSong(fileContent);
          if (response.error) {
            this.updateFile({
              ...file,
              pending: false,
              error: response.error,
            });
          } else {
            this.updateFile({
              ...file,
              song: response.song,
              warnings: response.warnings,
            });
            if (!this.props.pending) {
              this.saveSong(file.id, response.song);
            }
          }
          window.URL.revokeObjectURL(files[i].preview);
      };
      reader.onabort = () => console.log('file reading was aborted');
      reader.onerror = () => console.log('file reading has failed');
      reader.readAsText(files[i], 'UTF-8');
    }
  }

  countReadFiles() {
    return this.state.files.reduce((acc, file) => {
      return file.song || (!file.song && !file.pending && !file.success) ? acc+1 : acc;
    }, 0);
  }

  countReadErrors() {
    return this.state.files.reduce((acc, file) => {
      return !file.song && !file.pending && !file.success ? acc+1 : acc;
    }, 0);
  }

  countSaveFiles() {
    return this.state.files.reduce((acc, file) => {
      return file.song && !file.pending ? acc+1 : acc;
    }, 0);
  }

  countSaveErrors() {
    return this.state.files.reduce((acc, file) => {
      return file.song && !file.pending && !file.success ? acc+1 : acc;
    }, 0);
  }

  getErrorFiles() {
    return this.state.files.filter(file => file.error !== undefined)
  }

  render() {
    const nbFiles = this.state.files.length;
    const readCount = this.countReadFiles();
    const readErrors = this.countReadErrors();
    const saveCount = this.countSaveFiles();
    const saveErrors = this.countSaveErrors();
    return (
      <div id="importSongs">
        <Dropzone
          className="dropzone"
          onDrop={this.onDrop}
        >
          <p>Déplacez les fichiers à importer ou cliquez pour séléctionner des fichiers</p>
        </Dropzone>
        <div className="statsCategory">
          <h5>Analyse des fichiers</h5>
          <span className="statsValue">
            {readCount}/{nbFiles} Fichiers analysés
          </span>
          <span className="statsErrors">
            {readErrors} Fichiers invalides
          </span>
        </div>
        <div className="statsCategory">
          <h5>Sauvegarde sur le serveur</h5>
          <span className="statsValue">
            {saveCount}/{nbFiles - readErrors} Fichiers traités
          </span>
          <span className="statsErrors">
            {saveErrors} Erreurs à l'enregistrement
          </span>
        </div>
        <SongFileItemList
          files={this.getErrorFiles()}
        />
      </div>
    )
  }
}

ImportSong.propTypes = {
  saveSong: PropTypes.func.isRequired,
  pending: PropTypes.bool.isRequired,
  success: PropTypes.bool.isRequired,
  failure: PropTypes.object,
}

export default ImportSong;
