import React from 'react';
import {Modal, ModalHeader, ModalBody } from 'reactstrap';
import ImportSong from './ImportSong';

const ImportSongModal =({
    isOpen,
    toggle,
    saveSong,
    pending,
    success,
    failure,
    ...props,
}) => { return (
    <Modal isOpen={isOpen} toggle={toggle} size="lg">
        <ModalHeader toggle={toggle}>Importation de fichiers chordpro</ModalHeader>
        <ModalBody>
          <ImportSong
            saveSong={saveSong}
            pending={pending}
            success={success}
            failure={failure}
            {...props}
          />
        </ModalBody>
    </Modal>
)};

export default ImportSongModal;