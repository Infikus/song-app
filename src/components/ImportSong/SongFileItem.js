import React from 'react';
import { ListGroupItem, ListGroupItemHeading, ListGroupItemText } from 'reactstrap';
import style from './ImportSong.scss';

const SongFileItem = ({
  fileName,
  isFileValid,
  pending,
  success,
  error,
  warnings,
}) => {
  const color = pending ? 'default' : (success ? 'success' : 'danger')
  let content = null;
  if (!isFileValid && !pending) {
    content = <span>Fichier non valide : {error}</span>;
  } else if (isFileValid && !pending && !success) {
    content = <span>Erreur lors de l'enregistrement</span>;
  } else if (pending && !isFileValid) {
    content = (
      <ul>
        <li>Validation du fichier <i className="fa fa-spinner fa-spin"></i></li>
        <li className="disabled">Enregistrement</li>
      </ul>
    );
  } else if (pending && !success) {
    content = (
      <ul>
        <li>Fichier validé</li>
        <li>Enregistrement <i className="fa fa-spinner fa-spin"></i></li>
      </ul>
    );
  } else {
    content = (
      <ul>
        <li>Fichier validé</li>
        <li>Enregistrement validé</li>
      </ul>
    );
  }
  return (
    <ListGroupItem className="songFileItem" color={color}>
      <ListGroupItemHeading>{ fileName }</ListGroupItemHeading>
      { content }
    </ListGroupItem>
  )
};

export default SongFileItem;