import React from 'react';
import NavLink from 'react-router-dom/NavLink';

import moment from 'moment';

const SongListLine = ({
    songList
}) => (
    <tr>
        <td><NavLink to={ "/list/" + songList.id }>{ songList.name }</NavLink></td>
        <td>{ '' }</td>
        <td>{ moment(songList.date).format('DD-MM-YYYY') }</td>
        <td>{ songList.song_list_items ? songList.song_list_items.length : "0" }</td>
    </tr>
)

export default SongListLine;