import React, { Component } from 'react';
import { Table } from 'reactstrap';
import SongListLine from './SongListLine';

class SongListList extends Component {
  componentDidMount() {
    this.props.loadData();
  }

  render() {
    const { songLists } = this.props;
    console.log(songLists);
    return (
      <Table>
        <thead>
          <tr>
            <th>Nom</th>
            <th>Dirigeant</th>
            <th>Date</th>
            <th>Nombre de chants</th>
          </tr>
        </thead>
        <tbody>
          {
            songLists.map(songList => 
              songList && <SongListLine key={songList.id} songList={songList}/>
            )
          }
        </tbody>
      </Table>
    )
  }
}
export default SongListList;