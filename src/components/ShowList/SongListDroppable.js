import React, { Component } from 'react';

import { Droppable, Draggable } from 'react-beautiful-dnd';
import Measure from 'react-measure';
import { Card, CardBody, CardTitle, CardSubtitle, ListGroup, ListGroupItem, Button } from 'reactstrap';
import moment from 'moment';
import SongItem from './SongItem';

import style from './ShowList.scss';

class SongListDroppable extends Component {
  getSongParamsFromItem(item) {
    if (item.song_params && item.song_params.length > 0) {
      return item.song_params[0];
    } else if (item.song.song_params && item.song.song_params.length > 0) {
      return item.song.song_params[0];
    }
    return null;
  }

  render() {
    const {
      id,
      songList,
      songListItems,
      onDelete,
      onResize,
      onPreview,
      preview,
      onItemSelect,
      style,
      collapse,
      setCollapse,
      onCapoChange,
      capos,
      onTransposeChange,
      onDeleteSongList,
      onEditSongList,
    } = this.props;
    return (            
      <Measure
        bounds
        margin
        onResize={onResize}>
        {({ measureRef }) => (
        <div ref={measureRef} style={style} id='songListDroppable'>
          <CardBody className="cardHeader">
            <CardTitle>{ songList && songList.name + "  "}<i onClick={onEditSongList} className="fa fa-edit"></i></CardTitle>
            <CardSubtitle>{ songList && moment(songList.date).format('DD/MM/Y') }</CardSubtitle>
            <hr/>
            <Button size="sm" disabled={!songListItems || !songListItems.length } onClick={onPreview}>{preview ? "Mode édition" : "Mode preview"}</Button>
            <Button size="sm float-right" color="danger" onClick={onDeleteSongList}>Supprimer la liste</Button>
          </CardBody>
          <ListGroup className="listGroup">
            <Droppable droppableId={id} >
              {(provided, snapshot) => (
              <div ref={provided.innerRef} className='droppable'>
                { songListItems && songListItems.map((item, i) => (
                <Draggable key={item.id ? item.id : 'loading'+Math.random()} draggableId={item.id ? item.id : 'loading'+Math.random()}>
                  {(provided, snapshot) => (
                  <div>
                    <div ref={provided.innerRef} {...provided.dragHandleProps} style={provided.draggableStyle}>
                      <div className={'songItem'+(snapshot.isDragging ? ' isDragging' : '')}>
                        <SongItem 
                        onItemSelect={() => onItemSelect(item)}
                        onDelete={() => onDelete(item)}
                        song={item.song}
                        songParams={this.getSongParamsFromItem(item)}
                        pending={item.id === undefined}
                        collapse={collapse[item.id]}
                        setCollapse={(collapse) => setCollapse(collapse, item.id)}
                        onCapoChange={(capo, request = true) => onCapoChange(capo, item.id, request)}
                        capo={capos[item.id]}
                        onTransposeChange={(transpose, request = true) => onTransposeChange(transpose, item.id, request)}
                        transpose={item.transpose}
                        />
                      </div>
                    </div>
                    { provided.placeholder }
                  </div>
                  )}
                </Draggable>
                )) }
                { provided.placeholder}
              </div>
              )}
            </Droppable>
          </ListGroup>
        </div>
        )}
      </Measure>
    )
  }
}

export default SongListDroppable;