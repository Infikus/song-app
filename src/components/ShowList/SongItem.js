import React, { Component } from 'react';

import { ListGroupItem } from 'reactstrap';
import { Button, Collapse } from 'reactstrap';
import CapoInput from '../Song/CapoInput'
import TransposeInput from '../Song/TransposeInput';

class SongItem extends Component {

  constructor(props) {
    super(props);
    this.state = {
      capo: this.getCapo(),
    };
    this.props.onCapoChange(this.state.capo, false);
  }

  componentWillReceiveProps(nextProps) {
    const capo = this.getCapo(nextProps);
    if (capo != this.state.capo) {
      this.setState({
          capo,
        }, () => {
          this.props.onCapoChange(capo, false);
        }
      );
    }
  }

  getCapo(props = this.props) {
    const { songParams } = props;
    if (!songParams) {
      return 0;
    }
    return songParams.capo;
  }

  render() {
    const { style, song, onDelete, onItemSelect, collapse, setCollapse, pending, onCapoChange, onTransposeChange } = this.props;
    const { capo, transpose } = this.props;
    return (
      <ListGroupItem onClick={onItemSelect} style={style}>
        {song.title}
        <div className="float-right">
          { setCollapse && !pending ?
            <Button outline size="sm" onClick={() => setCollapse(!collapse)}>V</Button> :
            (setCollapse ? 'Loading...' : null)}
          {" "}
          { onDelete ? <Button outline size="sm" color="danger" onClick={onDelete}>X</Button> : null }
        </div>
        <Collapse isOpen={collapse ? true : false}>
          Capo :
          <CapoInput 
            value={capo}
            onChange={onCapoChange}
          />
          Transpose :
          <TransposeInput 
            initialKey={song.key}
            value={transpose}
            onChange={onTransposeChange}
          />
        </Collapse>

      </ListGroupItem>
    )
  }
}

SongItem.defaultProps = {
  pending: false,
  onCapoChange: () => {},
  onTransposeChange: () => {},
};

export default SongItem;