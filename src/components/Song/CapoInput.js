import React from 'react';
import PropTypes from 'prop-types';
import { Input } from 'reactstrap';

const CapoInput = ({
  value,
  onChange,
  name,
  ...props,
}) => (
  <Input
    style={{display: "inline"}}
    type="select"
    value={value}
    name={name}
    onChange={e => onChange(e.target.value)}
    {...props}
  >
    <option value={0}>0</option>
    <option value={1}>1</option>
    <option value={2}>2</option>
    <option value={3}>3</option>
    <option value={4}>4</option>
    <option value={5}>5</option>
    <option value={6}>6</option>
    <option value={7}>7</option>
    <option value={8}>8</option>
    <option value={9}>9</option>
    <option value={10}>10</option>
    <option value={11}>11</option>
    <option value={12}>12</option>
  </Input>
);

CapoInput.propTypes = {
  value: PropTypes.number,
  onChange: PropTypes.func,
  name: PropTypes.string,
};

CapoInput.defaultProps = {
  value: 0,
  onChange: () => {},
  name: 'capo',
};

export default CapoInput;
