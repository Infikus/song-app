import React from 'react';
import PropTypes from 'prop-types';
import { Input } from 'reactstrap';
import SongContent from './SongContent';
import { numberToChord } from '../../utils/chords';

const TransposeInput = ({
  initialKey,
  value,
  onChange,
  name,
  ...props,
}) => {

  let options = [];
  // Generate select's options
  for (let i = -6; i < 6; i += 1) {
    options[i+6] = (
      <option key={i} value={i}>
        {numberToChord(initialKey+i)}
        {i === 0 && ' (original)'}
        {i !== 0 && ` (${i < 0 ? i : '+'+i})`}
      </option>
    )
  }

  return (
    <Input
      style={{display: "inline"}}
      type="select"
      value={value}
      name={name}
      onChange={e => onChange(e.target.value)}
      {...props}
    >
      { options }
    </Input>
  )
};

TransposeInput.propTypes = {
  value: PropTypes.number,
  onChange: PropTypes.func,
  name: PropTypes.string,
  initialKey: PropTypes.number,
};

TransposeInput.defaultProps = {
  initialKey: 0,
  value: 0,
  onChange: () => {},
  name: 'transpose',
};

export default TransposeInput;
