import React from 'react';
import {Modal, ModalHeader, ModalBody } from 'reactstrap';
import CreateSongForm from '../../containers/Song/CreateSongForm';

const SongFormModal =({
    isOpen,
    toggle,
    SongForm,
    ...props,
}) => { return (
    <Modal isOpen={isOpen} toggle={toggle} size="lg">
        <ModalHeader toggle={toggle}>Modal title</ModalHeader>
        <ModalBody>
            <SongForm {...props}/>
        </ModalBody>
    </Modal>
)};

export default SongFormModal;