import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardBody, CardTitle, Label, Row, Button, Col } from 'reactstrap';
import CapoInput from './CapoInput';
import TransposeInput from './TransposeInput';

const SongParamsCard = ({
  transpose,
  capo,
  song,
  onCapoChanged,
  onTransposeChanged,
  onSave,
}) => (
  <Card>
    <CardBody>
        <CardTitle>Paramètres</CardTitle>
        <hr/>
        <Label htmlFor="capo">Capo</Label>
        <CapoInput
          value={capo}
          name="capo"
          onChange={onCapoChanged}
        />
        <TransposeInput
          value={transpose}
          initialKey={song ? song.key : 0}
          name="transpose"
          onChange={onTransposeChanged}
        />
        <Col>
          <Button onClick={onSave}>Enregistrer</Button>
        </Col>
    </CardBody>
  </Card>
);

SongParamsCard.propTypes = {
  capo: PropTypes.number.isRequired,
  transpose: PropTypes.number.isRequired,
  song: PropTypes.object,
  onCapoChanged: PropTypes.func.isRequired,
  onTransposeChanged: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
}

SongParamsCard.defaultProps = {
  song: {
    key: 0,
  }
}

export default SongParamsCard;
