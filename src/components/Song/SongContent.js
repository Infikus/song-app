import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { CardText } from 'reactstrap';
import style from './song.css';
import { chordToNumber, numberToChord } from '../../utils/chords';

class SongContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      htmlContent: this.transformContent(),
    };
    this.transformContent = this.transformContent.bind(this);
    this.chordMatchToHtml = this.chordMatchToHtml.bind(this);
  }

  preventChordOverlapping() {
    let phrases = document.getElementsByClassName('phrase');
    const minSpace = 10; // Minimal space between two chords
    for(let i = 0; i < phrases.length; i += 1) {
      let chord = phrases[i].getElementsByClassName('chord')[0];
      let content = phrases[i].getElementsByClassName('content')[0];
      let chordBox = chord.getBoundingClientRect();
      let wChord = chordBox.right - chordBox.left;
      let contentBox = content.getBoundingClientRect();
      let wContent = contentBox.right - contentBox.left;

      if (wChord + minSpace > wContent) {
        content.style.marginRight = `${-wContent + minSpace + wChord}px`
      }

      phrases[i].className += ' phrase-style';
    }
  }

  componentDidMount() {
    this.preventChordOverlapping();
  }

  componentDidUpdate() {
    this.preventChordOverlapping();
  }

  shouldComponentUpdate(nextProps) {
    return nextProps.content !== this.props.content ||
      this.props.capo !== nextProps.capo ||
      this.props.transpose !== nextProps.transpose;
  }

  chordMatchToHtml(c, p1, chord, content, capo, transpose) {
    let nChord = chord;
    console.log(transpose);
    if (nChord) {
      const chordRegex = /(A|B|C|D|E|F|G)(#|b)?(.*)/;
      nChord = chord.replace(chordRegex, (correspondance, letter, alt, rest) => {
        const numberVal = chordToNumber(letter, alt);
        console.log(numberVal, numberVal - capo + transpose);
        const chordWithCapo = numberToChord(numberVal - capo + transpose);
        return `${chordWithCapo}${rest}`;
      });
    }
    return `<span class='phrase'><span class='chord'>${nChord ? nChord : ''}</span><span class='content'>${content}</span></span>`;
  }


  /**
   * Transforme le format chordpro en format html
   */
  transformContent(props = this.props) {
    const { content, capo, transpose } = props;
    const chordRegex = /(\[([^\]\n\t]*)\])?([^\[\n\t]*)/g;
    const commentRegex= /\{(c|comment):([^\}\{\n\t]*)\}/g;
    const chorusRegex = /\{(soc|start_of_chorus)\}\n?([^]*?)\{(eoc|end_of_chorus)\}\n?/g;

    const brRegex = /([\n])/g

    let result = content.replace(chordRegex,
      (c, p1, chord, content) => this.chordMatchToHtml(c, p1, chord, content, capo, transpose));
    result = result.replace(commentRegex, "<span class='comment'>$2</span>");
    result = result.replace(chorusRegex, "<div class='chorus'>$2</div>");

    return result.replace(brRegex, '<br/>');
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.content !== this.props.content ||
      this.props.capo !== nextProps.capo ||
      this.props.transpose !== nextProps.transpose) {
      this.setState({
        htmlContent: this.transformContent(nextProps),
      });
    }
  }

  render() {
    const content = {__html: this.state.htmlContent};
    return (
      <div id="song">
        { this.props.capo ? <span className="capo">Capo: {this.props.capo}</span> : null } 
        <div className="chordline" dangerouslySetInnerHTML={content}></div>
      </div>
    );
  }
};

SongContent.propTypes = {
  capo: PropTypes.number,
  transpose: PropTypes.number,
};

SongContent.defaultProps = {
  capo: 0,
  transpose: 0,
}

export default SongContent;