import React from 'react';
import { Card, CardBody, CardTitle, CardSubtitle } from 'reactstrap';
import SongContent from './SongContent';

const Song = ({
  song,
  capo,
  transpose,
  pending,
  style
}) => {
  if (!pending && song) {
    return (
      <Card style={style}>
        <CardBody style={{overflow: "auto"}}>
          <CardTitle>{ song.title }</CardTitle>
          <CardSubtitle>{ song.author }</CardSubtitle>
          <hr/>
          <SongContent transpose={transpose} capo={capo} content={song.content} />
        </CardBody>
      </Card>
    )
  } else {
    return (
      <p>Pending....</p>
    )
  }
};

Song.defaultProps = {
  capo: 0,
}

export default Song;