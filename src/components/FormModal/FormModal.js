import React from 'react';
import {Modal, ModalHeader, ModalBody } from 'reactstrap';

const FormModal =({
    isOpen,
    toggle,
    Form,
    title,
    modalSize,
    style,
    ...props,
}) => { 
    return (
    <Modal style={style} isOpen={isOpen} toggle={toggle} size={ modalSize ? modalSize : "lg"}>
        <ModalHeader toggle={toggle}>{title}</ModalHeader>
        <ModalBody>
            <Form {...props}/>
        </ModalBody>
    </Modal>
)};

export default FormModal;