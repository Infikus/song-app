import React, {Component} from 'react';
import {
  Badge,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Dropdown
} from 'reactstrap';
import { select } from 'redux-saga/effects';

class TeamsDropdown extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  dropTeams(teams, selectedTeamId) {
    const selectedTeam = teams.find(t => t.id === selectedTeamId);
    const selectedTeamName = selectedTeam ? selectedTeam.name : 'Pas de groupe sélectionné';
    return (
      <Dropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle nav>
            {selectedTeamName}<i style={{marginLeft: "1em"}} className="fa fa-users"></i>
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem header tag="div" className="text-center"><strong>Mes groupes</strong></DropdownItem>
          {
            teams.map((team, index) => {
              if (!team.private) {
                return (
                  <DropdownItem 
                    onClick={() => this.props.setSelectedTeam(team.id)}
                    className={team.id == selectedTeamId ? "active" : ""} 
                    key={"team-dropdown-"+index}>
                    {team.name}
                  </DropdownItem>
                );
              } else return null;
            })
          }
          {
            teams.map((team, index) => {
              if (team.private) {
                return (
                  <DropdownItem 
                    onClick={() => this.props.setSelectedTeam(team.id)}
                    className={team.id == selectedTeamId ? "active" : ""} 
                    key={"team-dropdown-"+index}>
                      <i className="fa fa-lock"></i>{team.name}
                    </DropdownItem>
                );
              } else return null;
            })
          }
          <DropdownItem divider/>
          <DropdownItem onClick={this.props.onNewTeamClick}><i className="fa fa-plus"></i>Nouveau groupe</DropdownItem>
        </DropdownMenu>
      </Dropdown>
    );
  }

  render() {
    const {teams, selectedTeamId} = this.props;
    return (
      this.dropTeams(teams, selectedTeamId)
    );
  }
}

export default TeamsDropdown;
