import React from 'react';

import { Container } from 'reactstrap';

import style from './Loading.css';

const Loading = () => (
  <div id="loader">
    <i className="fa fa-spinner fa-pulse fa-3x"></i>
  </div>
);

export default Loading;