import React from 'react';
import ReactPaginate from 'react-paginate';

const SongListPagination = ({
  pageCount,
  onPageChange,
  page,
  pageRangeDisplayed,
  marginPagesDisplayed,
  nextLabel,
  previousLabel,
}) => (
  <div id="songListPagination">
    <ReactPaginate
      previousLabel={previousLabel}
      nextLabel={nextLabel}
      pageCount={pageCount}
      pageRangeDisplayed={pageRangeDisplayed}
      marginPagesDisplayed={marginPagesDisplayed}
      forcePage={page}
      onPageChange={(data) => onPageChange(data.selected)}
      breakLabel={<a href="#">...</a>}
      breakClassName={"break-me"}
      containerClassName={"pagination"}
      subContainerClassName={"pages pagination"}
      activeClassName={"active"}
    />
  </div>
);

SongListPagination.defaultProps = {
  pageRangeDisplayed: 3,
  marginPagesDisplayed: 2,
  nextLabel: "Suivant",
  previousLabel: "Précédent"
}

export default SongListPagination;