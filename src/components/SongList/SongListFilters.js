import React from 'react';
import { Col, Input, Label, FormGroup, Row } from 'reactstrap';

import style from './SongList.scss';
import { filterToString, FILTER_TITLE_ASC } from '../../reducers/app/me.reducer';

const SongListFilters = ({
  onSearchChange,
  search,
  onFilterChange,
  filter,
  showFilter,
}) => (
  <Row id="songListFilters">
    <Col md={showFilter ? "6" : "12"} sm="12" xs="12" className="filterItem alignLeft search">
      <Input
        type="text"
        placeholder="Rechercher"
        value={search}
        onChange={(e) => onSearchChange(e.target.value)}
      />
    </Col>

    { showFilter && 
      <Col md="6" sm="12" xs="12" className="filterItem alignRight sortBy">
        <div className="label">
          Trier par{" "}
        </div>
        <Input
          type="select"
          name="sortBy"
          value={filter}
          onChange={e => onFilterChange(e.target.value)}
        >
          {
            Object.keys(filterToString).map(filter => (
              <option value={filter} key={filter}>{filterToString[filter]}</option>
            ))
          }
        </Input>
      </Col>
    }
  </Row>
);

SongListFilters.defaultProps = {
  showFilter: false,
  filter: FILTER_TITLE_ASC,
  search: '',
  onFilterChange: () => {},
  onSearchChange: () => {},
}

export default SongListFilters;
