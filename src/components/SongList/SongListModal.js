import React from 'react';
import {Modal, ModalHeader, ModalBody } from 'reactstrap';

const SongListModal =({
    isOpen,
    toggle,
    SongForm,
    ...props,
}) => { return (
    <Modal isOpen={isOpen} toggle={toggle} size="lg">
        <ModalHeader toggle={toggle}>{title}</ModalHeader>
        <ModalBody>
            <SongForm {...props}/>
        </ModalBody>
    </Modal>
)};

export default SongListModal;