import React, { Component } from 'react';
import { Table } from 'reactstrap';
import ReactPaginate from 'react-paginate';
import SongLine from './SongLine';

import style from './SongList.scss';
import { FILTER_TITLE_ASC, FILTER_TITLE_DESC, FILTER_AUTHOR_DESC, FILTER_KEY_DESC, FILTER_AUTHOR_ASC, FILTER_KEY_ASC, FILTER_BPM_ASC, FILTER_BPM_DESC } from '../../reducers/app/me.reducer';

class SongList extends Component {
  constructor(props) {
    super(props);

    this.setSorting = this.setSorting.bind(this);
    this.renderSortIndicators = this.renderSortIndicators.bind(this);
  }

  componentWillMount() {
    this.props.loadData();
  }

  setSorting(field) {
    const curSort = this.props.filter;
    let filter = FILTER_TITLE_ASC;
    switch (field) {
      case 'title':
        if (curSort === FILTER_TITLE_ASC) {
          filter = FILTER_TITLE_DESC;
        } else {
          filter = FILTER_TITLE_ASC;
        }
        break;
      case 'author':
        if (curSort === FILTER_AUTHOR_ASC) {
          filter = FILTER_AUTHOR_DESC;
        } else {
          filter = FILTER_AUTHOR_ASC;
        }
        break;
      case 'key':
        if (curSort === FILTER_KEY_ASC) {
          filter = FILTER_KEY_DESC;
        } else {
          filter = FILTER_KEY_ASC;
        }
        break;
      case 'bpm':
        if (curSort === FILTER_BPM_ASC) {
          filter = FILTER_BPM_DESC;
        } else {
          filter = FILTER_BPM_ASC;
        }
        break;
    }
    this.props.onFilterChange(filter);
  }

  renderSortIndicators(field) {
    const sortAsc = <span key="up" className="caretUp"></span>;
    const sortDesc = <span key="down" className="caretDown"></span>;
    const noSort = [sortAsc, sortDesc];
    const { filter } = this.props;
    switch(field) {
      case 'title':
        if (filter === FILTER_TITLE_ASC) return sortAsc;
        if (filter === FILTER_TITLE_DESC) return sortDesc;
        break;
      case 'author':
        if (filter === FILTER_AUTHOR_ASC) return sortAsc;
        if (filter === FILTER_AUTHOR_DESC) return sortDesc;
        break;
      case 'key':
        if (filter === FILTER_KEY_ASC) return sortAsc;
        if (filter === FILTER_KEY_DESC) return sortDesc;
        break;
      case 'bpm':
        if (filter === FILTER_BPM_ASC) return sortAsc;
        if (filter === FILTER_BPM_DESC) return sortDesc;
        break;
    }
    return noSort
  }

  render() {
    return (
      <div>
        <Table>
          <thead>
            <tr>
              <th className="sort" onClick={() => this.setSorting('title')}>Titre {this.renderSortIndicators('title')}</th>
              <th className="sort" onClick={() => this.setSorting('author')}>Auteur {this.renderSortIndicators('author')}</th>
              <th className="sort" onClick={() => this.setSorting('key')}>Tonalité {this.renderSortIndicators('key')}</th>
              <th className="sort" onClick={() => this.setSorting('bpm')}>BPM {this.renderSortIndicators('bpm')}</th>
            </tr>
          </thead>
          <tbody>
            {
              this.props.songs.map(song => 
                <SongLine key={song.id} song={song}/>
              )
            }
          </tbody>
        </Table>
      </div>
    )
  }
}

export default SongList;