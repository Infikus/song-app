import React from 'react';
import NavLink from 'react-router-dom/NavLink';
import SongContent from '../Song/SongContent';
import { numberToChord } from '../../utils/chords';

const SongLine = ({
    song
}) => (
    <tr>
        <td><NavLink to={ "/song/" + song.id }>{ song.title }</NavLink></td>
        <td>{ song.author }</td>
        <td>{ numberToChord(song.key) }</td>
        <td>{ song.bpm }</td>
    </tr>
)

export default SongLine;