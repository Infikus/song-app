import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Alert } from 'reactstrap';

class ErrorMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showError: false,
      id: 0,
    };
    this.onDismiss = this.onDismiss.bind(this);
  }

  componentDidMount() {
    if (this.props.checkMount && this.props.show) {
      const id = this.state.id+1
      this.setState(prevState => ({
        showError: true,
        id: prevState.id + 1,
      }));
      if (this.props.timeout > 0)
        setTimeout(() => {
          if (this.state.id === id)
            this.onDismiss();
        }, this.props.timeout*1000);
    } 
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.show && !this.props.show) {
      const id = this.state.id+1;
      this.setState(prevState => ({
        showError: true,
        id: prevState.id + 1,
      }));
      if (nextProps.timeout > 0)
        setTimeout(() => {
          console.log(id, this.state);
          if (this.state.id === id)
            this.onDismiss();
        }, this.props.timeout*1000);
    } else if (!nextProps.show && this.props.show) {
      this.setState({
        showError: false,
      });
    }
  }

  onDismiss() {
    this.setState({
      showError: false,
    })
  }

  render() {
    const { color, children } = this.props;
    const { showError } = this.state;
    return (
      <Alert color={color} isOpen={showError} toggle={this.onDismiss}>
        { children }
      </Alert>
    );
  }
}

ErrorMessage.propTypes = {
  checkMount: PropTypes.bool,
  show: PropTypes.bool,
  color: PropTypes.string,
  children: PropTypes.string,
  timeout: PropTypes.number,
};

ErrorMessage.defaultProps = {
  checkMount: false,
  color: "danger",
  timeout: 5,
}

export default ErrorMessage;