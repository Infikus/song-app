import React from 'react';

import { Button } from 'reactstrap';
import style from './LoadingButton.css';

const LoadingButton = ({
  pending,
  children,
  disabled,
  ...props,
}) => (
  <Button {...props} disabled={disabled || pending}>
    <span className={pending ? "loadingButton" : ""}>
      { pending && <i className="loadingSpinner fa fa-spinner fa-spin"></i> }
      { children }
    </span>
  </Button>
);

export default LoadingButton;