import React from 'react';
import NavLink from 'react-router-dom/NavLink';

import { Table } from 'reactstrap';
import style from './teamList.css';

const TeamList = ({
  teams,
}) => (
  <Table responsive hover>
    <thead>
      <tr>
        <th colSpan={2}>Groupes existants :</th>
      </tr>
    </thead>
    <tbody>
      {
        teams.map(team => (
          <tr className="teamListItem" key={team.id}>
              <td><NavLink className="teamListLink" to={ "/settings/team/" + team.id }>{team.name}</NavLink></td>
              <td className="teamListDelete"><i className="fa fa-trash-o"></i></td>
          </tr>
        ))
      }
    </tbody>
  </Table>
);

export default TeamList;