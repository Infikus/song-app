import React, { Component } from 'react';

import { Table, Input } from 'reactstrap';
import { roleToString } from '../../reducers/entities/teamMembers.reducer';

class TeamMemberList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      roles:  {}
    };

    this.onRoleChange = this.onRoleChange.bind(this);
  }

  componentDidMount() {
    this.updateRoles(this.props);
  }

  componentWillReceiveProps(nextProps) {
    this.updateRoles(nextProps);
  }

  updateRoles(props) {
    if (!props.team.team_members)
      return ;
    const nRoles = Object.keys(props.team.team_members).reduce((acc, id) => {
      return {
        ...acc,
        [props.team.team_members[id].id]: props.team.team_members[id].role,
      };
    }, {});

    this.setState((prevState) => ({
      roles: nRoles,
    }));
  }

  onRoleChange(teamMemberId, role) {
    this.setState((prevState) => ({
      roles: {
        ...prevState.roles,
        [teamMemberId]: role,
      }
    }));
    this.props.onRoleChange(teamMemberId, role);
  }

  render() {
    const { team, canEdit } = this.props;
    return (
      <Table>
        <thead>
          <tr>
            <th colSpan={canEdit ? 3 : 2}>Membres du groupe :</th>
          </tr>
        </thead>
        <tbody>
          {
            team.team_members && 
            team.team_members.map((teamMember) => (
              <tr key={teamMember.id}>
                <td>{teamMember.user.email}</td>
                {
                  canEdit ?
                    <td>
                      <Input type="select" value={this.state.roles[teamMember.id]} onChange={(e) => this.onRoleChange(teamMember.id, e.target.value)}>
                        <option value={1}>{roleToString(1)}</option>
                        <option value={0}>{roleToString(0)}</option>
                      </Input>
                    </td>
                  :
                    <td>
                      {roleToString(teamMember.role)}
                    </td>
                }
                { 
                  canEdit && 
                  <td>
                    <i onClick={() => this.props.deleteTeamMember(teamMember)} className="fa fa-trash"></i>
                  </td>
                }
              </tr>
            ))
          }
        </tbody>
      </Table>
    )
  }
}

export default TeamMemberList;