import { isTokenValid } from '../sagas/tools';
import { getMe, isTokenRefreshing } from '../reducers/rootSelectors';
import { refreshTokenRequest, addWaitingAction, REFRESH_TOKEN_REQUEST, REFRESH_TOKEN_SUCCESS, ADD_WAITING_ACTION, LOGOUT } from '../actions/users.action';

// List of actions that should not add in waitingActions array
const authorizedActions = [
  REFRESH_TOKEN_REQUEST,
  REFRESH_TOKEN_SUCCESS,
  ADD_WAITING_ACTION,
  LOGOUT,
];

const shouldIgnoreAction = (action) =>
  authorizedActions.indexOf(action.type) >= 0 ||
  action.type.includes('@@') || // For react router for instance
  action.type.includes('FAILURE') ||
  action.type.includes('SUCCESS');

export default function authMiddleware({ dispatch, getState }) {
  return (next) => (action) => {
    let state = getState();
      if(!shouldIgnoreAction(action)) {
        // make sure we are not already refreshing the token
        const isRefreshing = isTokenRefreshing(state);
        if(isRefreshing) {
          dispatch(addWaitingAction(action));
          return ;
        }
      }
    return next(action);
  };
};
