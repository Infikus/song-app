import React from 'react';
import { withFormik, Formik, validateYupSchema } from 'formik';
import Yup from 'yup';
import { ErrorInput, getError } from './components/ErrorInput';
import { Input, Form, Label, Button, FormGroup, Col, Row, InputGroup, InputGroupAddon } from 'reactstrap';
import LoadingButton from '../components/Common/LoadingButton';

const validationSchema = Yup.object().shape({
    email: Yup.string().email("Veuillez entrer une adresse mail valide.").required("Ce champ est requis."),
    password: Yup.string().min(8, "8 caractères minimum").required("Ce champ est requis."),
    passwordCheck: Yup.mixed().oneOf([
        Yup.ref("password")
    ], "Les deux mots de passe sont differents.").required("Ce champ est requis."),
});

const RegisterForm = ({
    onSubmit,
    afterSubmit,
    initialValues,
    apiErrors,
    pending,
}) => (
    <Formik
        validationSchema={validationSchema}
        onSubmit={(v, p) => { onSubmit(v, p); if (afterSubmit) afterSubmit(v, p); }}
        initialValues={initialValues}
        render={
            ({
                values,
                errors, 
                handleSubmit,
                touched,
                handleChange, 
                handleBlur,
                setFieldValue,
                isValid,
                ...props
            }) => (
                <Form onSubmit={handleSubmit}>
                    <div className="mb-4">
                        <InputGroup >
                            <InputGroupAddon style={{
                                color: getError(errors, touched, "email") ? 'red' : null,
                                borderColor: getError(errors, touched, "email") ? 'red' : null,
                                backgroundColor: getError(errors, touched, "email") ? '#FFD5D5' : null,
                            }}><i className="icon-user"></i></InputGroupAddon>
                            <Input 
                                placeholder="Email"
                                type="text"
                                value={values.email ? values.email : ""}
                                id="email" name="email"
                                valid={getError(errors, touched, "email") ? false : null} 
                                onChange={handleChange} 
                                onBlur={handleBlur}
                                />

                        </InputGroup>
                        <div style={{
                            color: "red",
                            fontSize: "0.8em",
                            textAlign: "center",
                        }}>{ getError(errors, touched, "email") }</div>
                    </div>
                    <div className="mb-4">
                        <InputGroup >
                            <InputGroupAddon style={{
                                color: getError(errors, touched, "password") ? 'red' : null,
                                borderColor: getError(errors, touched, "password") ? 'red' : null,
                                backgroundColor: getError(errors, touched, "password") ? '#FFD5D5' : null,
                            }}><i className="icon-user"></i></InputGroupAddon>
                            <Input 
                                placeholder="Mot de passe"
                                type="password"
                                value={values.password ? values.password : ""}
                                id="password" name="password"
                                valid={getError(errors, touched, "password") ? false : null} 
                                onChange={handleChange} 
                                onBlur={handleBlur}
                                />

                        </InputGroup>
                        <div style={{
                            color: "red",
                            fontSize: "0.8em",
                            textAlign: "center",
                        }}>{ getError(errors, touched, "password") }</div>
                    </div>
                    <div className="mb-4">
                        <InputGroup >
                            <InputGroupAddon style={{
                                color: getError(errors, touched, "passwordCheck") ? 'red' : null,
                                borderColor: getError(errors, touched, "passwordCheck") ? 'red' : null,
                                backgroundColor: getError(errors, touched, "passwordCheck") ? '#FFD5D5' : null,
                            }}><i className="icon-user"></i></InputGroupAddon>
                            <Input 
                                placeholder="Confirmer mot de passe"
                                type="password"
                                value={values.passwordCheck ? values.passwordCheck : ""}
                                id="passwordCheck" name="passwordCheck"
                                valid={getError(errors, touched, "passwordCheck") ? false : null} 
                                onChange={handleChange} 
                                onBlur={handleBlur}
                                />

                        </InputGroup>
                        <div style={{
                            color: "red",
                            fontSize: "0.8em",
                            textAlign: "center",
                        }}>{ getError(errors, touched, "passwordCheck") }</div>
                    </div>
                    <Row>
                      <Col xs="6">
                        <LoadingButton pending={pending} color="primary" disabled={!isValid} type="submit" className="px-4">Créer le compte</LoadingButton>
                      </Col>
                    </Row>
                </Form>
            )
        }
    />
)

export default RegisterForm;
