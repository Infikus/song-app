import React from 'react';
import { withFormik, Formik, validateYupSchema } from 'formik';
import Yup from 'yup';
import { ErrorInput, getError } from './components/ErrorInput';
import { Input, Form, Label, Button, FormGroup, Col, Row } from 'reactstrap';
import LoadingButton from '../components/Common/LoadingButton';


const validationSchema = Yup.object().shape({
    title: Yup.string()
        .min(2, "Le titre doit faire au moins 2 caractères.")
        .max(255, "Le titre ne doit pas faire plus de 255 caractères.")
        .required("Le titre est requis."),
    author: Yup.string().nullable(true),
    content: Yup.string()
        .required("Veuillez entrer les paroles du chant."),
    bpm: Yup
        .number()
        .integer("Veuillez entrer un nombre entre 0 et 300.")
        .min(0, "Valeur minimale : 0.")
        .max(300, "Valeur maximale : 300")
        .nullable(true),
    key: Yup.number().integer("Valeur incorrecte.")
        .min(0, "Valeur incorrecte.")
        .max(11, "Valeur incorrecte.")
        .required("Veuillez donner une valeur"),
    time_sig: Yup.string().nullable(true),
});

const SongForm = ({
    onSubmit,
    afterSubmit,
    initialValues,
    pending,
}) => (
    <Formik
        validationSchema={validationSchema}
        onSubmit={(v, p) => { onSubmit(v, p); if (afterSubmit) afterSubmit(v, p); }}
        initialValues={initialValues}
        isInitialValid={true}
        render={
            ({
                values,
                errors, 
                handleSubmit,
                touched,
                handleChange, 
                handleBlur,
                isValid,
                ...props
            }) => (
                <Form onSubmit={handleSubmit}>
                    <Row>
                        <Col xs={12} md={6}>
                            <FormGroup row>
                                <Label htmlFor="title" xs={4} md={3}>Titre</Label>
                                <Col xs={8} md={9}>
                                    <ErrorInput 
                                        component={Input} 
                                        value={values.title ? values.title : ""}
                                        id="title" name="title"
                                        error={getError(errors, touched, "title")} 
                                        onChange={handleChange} 
                                        onBlur={handleBlur}/>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs={12} md={6}>
                            <FormGroup row>
                                <Label htmlFor="author" xs={4} md={3}>Auteur(s)</Label>
                                <Col xs={8} md={9}>
                                    <ErrorInput 
                                        component={Input} 
                                        value={values.author ? values.author : ""}
                                        id="author" name="author"
                                        error={getError(errors, touched, "author")} 
                                        onChange={handleChange} 
                                        onBlur={handleBlur}/>
                                </Col>
                            </FormGroup>
                        </Col>
                    </Row>

                    <Row>
                        <Col xs={12} md={4}>
                            <FormGroup row>
                                <Label htmlFor="key" xs={4} md={3}>Tonalité</Label>
                                <Col xs={8} md={9}>
                                    <ErrorInput 
                                        component={Input} 
                                        type="select"
                                        id="key" name="key"
                                        value={values.key}
                                        error={getError(errors, touched, "key")} 
                                        onChange={handleChange} 
                                        onBlur={handleBlur}>
                                        <option value={0}>A</option>
                                        <option value={1}>Bb</option>
                                        <option value={2}>B</option>
                                        <option value={3}>C</option>
                                        <option value={4}>C#</option>
                                        <option value={5}>D</option>
                                        <option value={6}>D#</option>
                                        <option value={7}>E</option>
                                        <option value={8}>F</option>
                                        <option value={9}>F#</option>
                                        <option value={10}>G</option>
                                        <option value={11}>G#</option>
                                    </ErrorInput>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs={12} md={4}>
                            <FormGroup row>
                                <Label htmlFor="bpm" xs={4} md={3}>BPM</Label>
                                <Col xs={8} md={9}>
                                    <ErrorInput 
                                        component={Input} 
                                        value={values.bpm ? values.bpm : 0}
                                        type="number" min={0}
                                        id="bpm" name="bpm"
                                        error={getError(errors, touched, "bpm")} 
                                        onChange={handleChange} 
                                        onBlur={handleBlur}/>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs={12} md={4}>
                            <FormGroup row>
                                <Label htmlFor="time_sig" xs={4} md={3}>Signature</Label>
                                <Col xs={8} md={9}>
                                    <ErrorInput 
                                        component={Input} 
                                        value={values.time_sig ? values.time_sig : ""}
                                        id="time_sig" name="time_sig"
                                        error={getError(errors, touched, "time_sig")} 
                                        onChange={handleChange} 
                                        onBlur={handleBlur}/>
                                </Col>
                            </FormGroup>
                        </Col>
                    </Row>

                    <hr/>
                    
                    <FormGroup>
                        <Label htmlFor="content">Paroles et accords</Label>
                        <ErrorInput 
                            component={Input} 
                            type="textarea"
                            id="content" name="content"
                            value={values.content}
                            rows={15}
                            error={getError(errors, touched, "content")} 
                            onChange={handleChange} 
                            onBlur={handleBlur}/>
                    </FormGroup>

                    <LoadingButton pending={pending} type="submit" disabled={!isValid}>Valider</LoadingButton>
                </Form>
            )
        }
    />
)

export default SongForm;
