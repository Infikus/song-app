import React from 'react';
import { withFormik, Formik, validateYupSchema } from 'formik';
import Yup from 'yup';
import { Input, Form, Label, Button, FormGroup, Col, Row } from 'reactstrap';
import Moment from 'moment';
import momentLocalizer from 'react-widgets-moment';
import { DateTimePicker } from 'react-widgets';
import 'react-widgets/dist/css/react-widgets.css';

import { ErrorInput, getError } from './components/ErrorInput';
import LoadingButton from '../components/Common/LoadingButton';

Moment.locale('fr');
momentLocalizer();

const validationSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, "Le nom doit faire au moins 2 caractères.")
    .max(255, "Le nom ne doit pas faire plus de 255 caractères.")
    .required("Le nom est requis."),
  date: Yup.date("Format incorrect.").required("La date est requise."),
});

const NewListForm = ({
  onSubmit,
  afterSubmit,
  initialValues,
  pending,
}) => (
  <Formik
    validationSchema={validationSchema}
    onSubmit={(v, p) => { onSubmit(v, p); if (afterSubmit) afterSubmit(v, p); }}
    initialValues={initialValues}
    render={
      ({
        values,
        errors, 
        handleSubmit,
        touched,
        handleChange, 
        handleBlur,
        setFieldValue,
        isValid,
        ...props
      }) => (
        <Form onSubmit={handleSubmit}>
          <Row>
            <Col xs={12} md={6}>
              <FormGroup row>
                <Label htmlFor="name" xs={4} md={2}>Nom</Label>
                <Col xs={8} md={10}>
                  <ErrorInput 
                    component={Input} 
                    value={values.name ? values.name : ""}
                    id="name" name="name"
                    error={getError(errors, touched, "name")} 
                    onChange={handleChange} 
                    onBlur={handleBlur}/>
                </Col>
              </FormGroup>
            </Col>
            <Col xs={12} md={6}>
              <FormGroup row>
                <Label htmlFor="date" xs={4} md={2}>Date</Label>
                <Col xs={8} md={10}>
                  <ErrorInput 
                    component={DateTimePicker} 
                    value={values.date}
                    time={false}
                    id="date" name="date"
                    error={getError(errors, touched, "date")} 
                    onChange={(val) => setFieldValue('date', val)}
                    />
                </Col>
              </FormGroup>
            </Col>
          </Row>
          <LoadingButton pending={pending} type="submit" disabled={!validationSchema.isValidSync(values)}>Valider</LoadingButton>
        </Form>
      )
    }
  />
)

export default NewListForm;
