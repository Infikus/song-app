import React from 'react';
import { FormFeedback, InputGroupAddon, InputGroup } from 'reactstrap';

function getError(errors, touched, id) {
    return touched[id] && errors[id] ? errors[id] : "";
}

const InputFeedback = ({
    error
}) => error ? (
    <FormFeedback>{ error }</FormFeedback>
) : null;

const ErrorInput = ({
    component,
    id,
    error,
    children,
    style,
    ...props
}) => {
    const InputComponent = component;
    return (
        <div style={{width: "100%"}}>
            <InputComponent style={style} id={id} valid={ error ? false : null} {...props}>{children}</InputComponent>  
            <InputFeedback error={ error } />
        </div>
    )
}

export { ErrorInput, getError };