import React from 'react';
import { withFormik, Formik, validateYupSchema } from 'formik';
import Yup from 'yup';
import { ErrorInput, getError } from './components/ErrorInput';
import { Input, Form, Label, Button, FormGroup, Col, Row, InputGroup, InputGroupAddon } from 'reactstrap';
import LoadingButton from '../components/Common/LoadingButton';

const validationSchema = Yup.object().shape({
    email: Yup.string().email("Veuillez entrer un email valide.")
        .required("Le nom est requis."),
    role: Yup.number().integer().required("Rôle requis."),
});

const AddMemberForm = ({
    onSubmit,
    afterSubmit,
    initialValues,
    pending,
}) => (
    <Formik
        validationSchema={validationSchema}
        onSubmit={(v, p) => { onSubmit(v, p); if (afterSubmit) afterSubmit(v, p); }}
        initialValues={initialValues}
        isInitialValid={true}
        render={
            ({
                values,
                errors, 
                handleSubmit,
                touched,
                handleChange, 
                handleBlur,
                setFieldValue,
                isValid,
                ...props
            }) => (
                <Form onSubmit={handleSubmit}>
                    <Row>
                        <Col xs={12} md={6}>
                            <FormGroup row>
                                <Label htmlFor="name" md={3}>Email</Label>
                                <Col md={9}>
                                    <ErrorInput 
                                        component={Input} 
                                        value={values.email}
                                        id="email" name="email"
                                        error={getError(errors, touched, "email")} 
                                        onChange={handleChange} 
                                        onBlur={handleBlur}
                                    />
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs={12} md={6}>
                            <FormGroup row>
                                <Label htmlFor="role" md={3}>Rôle</Label>
                                <Col md={9}>
                                    <ErrorInput 
                                        component={Input} 
                                        type="select"
                                        id="role" name="role"
                                        value={values.role}
                                        error={getError(errors, touched, "role")} 
                                        onChange={handleChange} 
                                        onBlur={handleBlur}>
                                        <option value={0}>Responsable</option>
                                        <option value={1}>Membre</option>
                                    </ErrorInput>
                                </Col>
                            </FormGroup>
                        </Col>
                    </Row> 
                    <LoadingButton pending={pending} type="submit" disabled={!isValid} className={"float-right"}>Enregistrer</LoadingButton>
                </Form>
            )
        }
    />
)

export default AddMemberForm;
