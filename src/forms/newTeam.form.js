import React from 'react';
import { withFormik, Formik, validateYupSchema } from 'formik';
import Yup from 'yup';
import { ErrorInput, getError } from './components/ErrorInput';
import { Input, Form, Label, Button, FormGroup, Col, Row } from 'reactstrap';
import LoadingButton from '../components/Common/LoadingButton';

const validationSchema = Yup.object().shape({
    name: Yup.string()
        .min(2, "Le nom doit faire au moins 2 caractères.")
        .max(255, "Le nom ne doit pas faire plus de 255 caractères.")
        .required("Le nom est requis."),
});

const NewTeamForm = ({
    onSubmit,
    afterSubmit,
    initialValues,
    pending,
}) => (
    <Formik
        validationSchema={validationSchema}
        onSubmit={(v, p) => { onSubmit(v, p); if (afterSubmit) afterSubmit(v, p); }}
        initialValues={initialValues}
        validateOnChange={true}
        render={
            ({
                values,
                errors, 
                handleSubmit,
                touched,
                handleChange, 
                handleBlur,
                setFieldValue,
                isValid,
                ...props
            }) => (
                <Form onSubmit={handleSubmit}>
                    <Row>
                        <Col xs={12}>
                            <FormGroup row>
                                <Label htmlFor="name" xs={4} md={2}>Nom</Label>
                                <Col xs={8} md={10}>
                                    <ErrorInput 
                                        component={Input} 
                                        value={values.name}
                                        id="name" name="name"
                                        error={getError(errors, touched, "name")} 
                                        onChange={handleChange} 
                                        onBlur={handleBlur}/>
                                </Col>
                            </FormGroup>
                        </Col>
                    </Row> 
                    <LoadingButton pending={pending} type="submit" disabled={!validationSchema.isValidSync(values)}>Valider</LoadingButton>
                </Form>
            )
        }
    />
)

export default NewTeamForm;
