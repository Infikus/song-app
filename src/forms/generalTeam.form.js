import React from 'react';
import { withFormik, Formik, validateYupSchema } from 'formik';
import Yup from 'yup';
import { ErrorInput, getError } from './components/ErrorInput';
import { Input, Form, Label, Button, FormGroup, Col, Row, InputGroup, InputGroupAddon } from 'reactstrap';
import LoadingButton from '../components/Common/LoadingButton';

const validationSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, "Le nom doit faire au moins 2 caractères.")
    .max(255, "Le nom ne doit pas faire plus de 255 caractères.")
    .required("Le nom est requis."),
  description: Yup.string()
});

const GeneralTeamForm = ({
  onSubmit,
  afterSubmit,
  initialValues,
  apiErrors,
  onDeleteClick,
  canEdit,
  deletePending,
  savePending,
}) => (
  <Formik
    validationSchema={validationSchema}
    onSubmit={(v, p) => { onSubmit(v, p); if (afterSubmit) afterSubmit(v, p); }}
    initialValues={initialValues}
    enableReinitialize={true}
    isInitialValid={true}
    render={
      ({
        values,
        errors, 
        handleSubmit,
        touched,
        handleChange, 
        handleBlur,
        setFieldValue,
        isValid,
        ...props
      }) => (

        <Form onSubmit={handleSubmit}>
          <Row>
            <Col xs={12}>
              <FormGroup row>
                <Label htmlFor="name" xs={12} md={1}>Nom</Label>
                <Col xs={12} md={11}>
                  <ErrorInput 
                    component={Input} 
                    value={values.name}
                    disabled={!canEdit}
                    id="name" name="name"
                    error={getError(errors, touched, "name")} 
                    onChange={handleChange} 
                    onBlur={handleBlur}/>
                </Col>
              </FormGroup>
            </Col>
            <Col xs={12}>
              <FormGroup>
                <Label htmlFor="description">Description</Label>
                <ErrorInput 
                  component={Input} 
                  type="textarea"
                  rows={8}
                  disabled={!canEdit}
                  value={values.description}
                  id="description" name="description"
                  onChange={handleChange} 
                  onBlur={handleBlur}/>
              </FormGroup>
            </Col>
          </Row> 
          {
            canEdit && 
              <div>
                <LoadingButton pending={deletePending} onClick={onDeleteClick}>Supprimer</LoadingButton>
                <LoadingButton pending={savePending} type="submit" disabled={!isValid}>Enregistrer</LoadingButton>
              </div>
          }
        </Form>
      )
    }
  />
)

export default GeneralTeamForm;
