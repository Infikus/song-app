import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getSongById } from '../reducers/songs.reducer';
import {
  Card, Col, Row, CardBody, CardTitle, CardHeader, Button, Modal, ModalHeader, ModalBody, ModalFooter, CardSubtitle
} from 'reactstrap';
import SongContent from '../components/Song/SongContent';
import SongParamsCard from '../components/Song/SongParamsCard';
import { getSong, deleteSongRequest } from '../actions/songs.action';
import SongFormModal from '../components/Song/SongFormModal';
import CreateSongForm from '../containers/Song/CreateSongForm';
import UpdateSongForm from '../containers/Song/UpdateSongForm';
import { getSelectedTeam } from '../reducers/users.reducer';
import { getCurrentSong, isSongPending, getSelectedTeamId } from '../reducers/rootSelectors';
import { setSongParamsRequest } from '../actions/songParams.action';

class ShowSong extends Component {
  constructor(props) {
    super(props);
    this.state = {
      songModal: false,
      capo: this.getCapo(),
      transpose: this.getTranspose(),
    }

    this.toggleSongModal = this.toggleSongModal.bind(this);
    this.getCapo = this.getCapo.bind(this);
    this.getTranspose = this.getTranspose.bind(this);
    this.onCapoChanged = this.onCapoChanged.bind(this);
    this.onTransposeChanged = this.onTransposeChanged.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  toggleSongModal() {
    this.setState({
      songModal: !this.state.songModal,
    });
  }

  componentWillReceiveProps(nextProps, nextState) {
    if (nextProps.song) {
      if (!this.props.song || this.props.pending && !nextProps.pending) {
        this.setState({
          capo: this.getCapo(nextProps),
          transpose: this.getTranspose(nextProps),
        });
      }
    }

    // The selected team has changed
    if (nextProps.selectedTeamId !== this.props.selectedTeamId) {
      this.props.history.push('/lists');
    }
  }

  componentDidMount() {
    this.props.loadSong(this.props.match.params.songId);
  }

  getCapo(props = this.props) {
    const { song } = props;
    // If there is no songParams for the current user, set the capo to 0
    if (!song || !song.song_params || song.song_params.length === 0) {
      return 0;
    }
    return song.song_params[0].capo;
  }

  getTranspose(props = this.props) {
    const { song } = props;
    // If there is no songParams for the current user, set the transpose value to 0
    if (!song || !song.song_params || song.song_params.length === 0) {
      return 0;
    }
    return song.song_params[0].transpose;
  }

  onCapoChanged(nCapo) {
    this.setState({
      capo: parseInt(nCapo),
    });
  }

  onTransposeChanged(nTranspose) {
    // TODO: Modifier le capo pour concerver la même tona de jeu si possible
    this.setState({
      transpose: parseInt(nTranspose),
    });
  }

  onSave() {
    const { capo, transpose } = this.state;
    const { song } = this.props;
    const songParams = song && song.song_params && song.song_params.length > 0 ?
      song.song_params[0] :
      {};

    const newSongParams = {
      ...songParams,
      capo,
      transpose,
    };

    this.props.setSongParams(newSongParams, song);
  }

  render() {
    const { song, pending, deleteSong } = this.props;
    const { capo, transpose } = this.state;
    const content = (song && !pending) ?
      (
        <div>
          <CardTitle>{ song.title }</CardTitle>
          <CardSubtitle>{ song.author }</CardSubtitle>
          <Button onClick={this.toggleSongModal}>Editer</Button>
          <Button color="danger" className="float-right" onClick={() => deleteSong(song)}>Supprimer</Button>
          <hr/>
          <SongContent content={song.content} capo={capo} transpose={transpose} />
        </div>
      ) :
      (<div>Pending</div>);
    return (
      <div className="animated fadeIn">
        <Row>
          <Col lg="9">
            <Card>
              <CardBody>
                {content}
              </CardBody>
            </Card>
          </Col>
          <Col lg="3">
            <SongParamsCard
              capo={capo}
              transpose={transpose}
              song={song}
              onCapoChanged={this.onCapoChanged}
              onTransposeChanged={this.onTransposeChanged}
              onSave={this.onSave}
            />
          </Col>
        </Row>
        <SongFormModal
          SongForm={UpdateSongForm}
          isOpen={this.state.songModal}
          toggle={this.toggleSongModal}
          initialValues={song}
          afterSubmit={ () => { this.toggleSongModal(); }}
          />
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  song: getCurrentSong(state),
  pending: isSongPending(state),
  selectedTeamId: getSelectedTeamId(state),
});

const mapDispatchToProps = (dispatch) => ({
  loadSong: (songId) => {
    dispatch(getSong(songId));
  },
  deleteSong: (song) => {
    dispatch(deleteSongRequest(song));
  },
  setSongParams: (songParams, song) => {
    dispatch(setSongParamsRequest(songParams, song));
  },
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ShowSong));