import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getSongList, updateSongList, deleteSongListRequest } from '../actions/songLists.action';
import { setSongParamsRequest } from '../actions/songParams.action';
import { getSongs } from '../actions/songs.action';
import { withRouter } from 'react-router-dom'
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { Col, Row } from 'reactstrap';
import Measure from 'react-measure';
import moment from 'moment';
import SongsDraggable from '../containers/SongList/SongsDraggable.container';
import SongListDroppable from '../components/ShowList/SongListDroppable';
import Song from '../components/Song/Song';
import { 
  getAllSongs,
  getSelectedTeamId,
  getSongsOfSongList,
  getSongList as getSongListSelect,
  getSelectedTeamSongs,
  isPending,
  isSuccessful
} from '../reducers/rootSelectors';
import { updateSongListItemRequest } from '../actions/songListItems.action';
import FormModal from '../components/FormModal/FormModal';
import UpdateListForm from '../containers/Song/UpdateListForm';

class ShowSongList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      songList: null,
      pending: true,
      preview: false,
      boxSize: {
        width: -1,
        height: -1,
        top: -1,
      },
      windowHeight: 0,
      selectedItemId: undefined,
      collapse: {},
      capos: {},
      search: '',
      updateSongListModal: false,
    }

    this.onDragEnd = this.onDragEnd.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onResize = this.onResize.bind(this);
    this.updateWindowHeight = this.updateWindowHeight.bind(this);
    this.togglePreview = this.togglePreview.bind(this);
    this.onItemSelect = this.onItemSelect.bind(this);
    this.setCollapse = this.setCollapse.bind(this);
    this.onCapoChange = this.onCapoChange.bind(this);
    this.onTransposeChange = this.onTransposeChange.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
    this.onEditSongList = this.onEditSongList.bind(this);
    this.toggleSongListModal = this.toggleSongListModal.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    let updatedState = {};
    // If the fetch_song_list has succeeded
    if ((this.props.songListPending && !nextProps.songListPending && nextProps.songListSuccess) ||
      (this.props.songList && nextProps.songList && this.props.songList.song_list_items.length !== nextProps.songList.song_list_items.length)) {
      updatedState = {
        ...updatedState,
        songList: nextProps.songList,
      };
    } else if (nextProps.songList) {
      updatedState = {
        ...updatedState,
        songList: {
          ...this.state.songList,
          date: nextProps.songList.date,
          name: nextProps.songList.name,
        },
      };
    }

    let shouldUpdateSelectedItem = this.state.selectedItemId === undefined;
    const songListItems = nextProps.songList ? nextProps.songList.song_list_items : [];
    if (!shouldUpdateSelectedItem && nextProps.songList) {
      // Check if the selectedItemId is valid
      let idFound = false;
      for (let i = 0; i < songListItems.length; i += 1) {
        if (songListItems[i].id === this.state.selectedItemId) {
          idFound = true;
          break;
        }
      }
      if (!idFound) {
        shouldUpdateSelectedItem = true;
      }
    }

    if (shouldUpdateSelectedItem) {
      if (!nextProps.songList || songListItems.length === 0) {
        updatedState = {
          ...updatedState,
          selectedItemId: undefined,
        };
      } else {
        updatedState = {
          ...updatedState,
          selectedItemId: songListItems[0].id,
        };
      }
    }

    // The selected team has changed
    if (nextProps.selectedTeamId !== this.props.selectedTeamId) {
      this.props.history.push('/lists');
    }

    if (updatedState !== {}) {
      this.setState(updatedState);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const compareObjects = (a, b) =>
      Object.keys(a).length === Object.keys(b).length &&
      Object.keys(a).reduce((acc, k) => acc && a[k] === b[k], true);
    return nextState.preview !== this.state.preview ||
      !compareObjects(this.state.boxSize, nextState.boxSize) ||
      !compareObjects(this.state.capos, nextState.capos) ||
      !compareObjects(this.state.collapse, nextState.collapse) ||
      this.state.selectedItemId !== nextState.selectedItemId ||
      this.state.updateSongListModal !== nextState.updateSongListModal ||
      nextProps !== this.props;
  }

  componentDidMount() {
    // To calculate the size of the fixed box
    this.updateWindowHeight();
    window.addEventListener('resize', this.updateWindowHeight);

    this.props.loadSongList(this.props.match.params.songListId);
    this.props.loadSongs();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowHeight);
  }

  togglePreview() {
    this.setState((prevState) => ({
      preview: !prevState.preview,
    }));
  }
  
  updateWindowHeight() {
    this.setState({ windowHeight: window.innerHeight });
  }

  getSongListFromArray(songs) {
    // Create a real songList object to send
    const songListItems = songs.map((song, index) => {
      return {
        id: song.songListItemId,
        position: index,
        song: {
          ...song,
          team: {}, // to fix a serialization error
          song_params: null,
        }
      }
    });
    return {
      ...this.props.songList,
      team:{}, // to fix a serialization error
      song_list_items: songListItems,
    };
  }

  onDelete(songListItem) {
    const songListItems = this.state.songList.song_list_items;
    const index = this.state.songList.song_list_items.findIndex((e) => e.id === songListItem.id);
    const newItems = [...songListItems.slice(0, index), ...songListItems.slice(index+1)];

    const newSelectedId = this.state.selectedItemId === songListItem.id ?
      (newItems.length > 0 ? 
        newItems[0].id :
        undefined) :
      this.state.selectedItemId;

    this.setState((prevState) => ({
        selectedItemId: newSelectedId,
        preview: prevState.preview ? (newSelectedId === undefined ? false : true) : false,
        songList: {
          ...prevState.songList,
          song_list_items: newItems,
        },
      }), () => {
        this.props.updateList(this.state.songList)
      });
  }

  onResize(contentRect) {
    const top = this.state.boxSize.top >= 0 ? this.state.boxSize.top : contentRect.bounds.top;
    this.setState((prevState) => {
      return {
        boxSize: {
          width: contentRect.bounds.width,
          height: prevState.windowHeight - top - 50 - 24, // 50 = height of the footer. 24 = margin bottom
          top: top,
        },  
      }
    });
  }

  updatePosition(songListItems) {
    return songListItems.map((item, index) => ({
        ...item,
        position: index,
      })
    );
  }

  onDragEnd(result) {
    if (!result || !result.destination) {
      return ;
    }
    let newItems = this.state.songList.song_list_items.slice(0); // Copy the current state songList array
    // If a song is just move, we need to remove the old elemnent
    if (result.source.droppableId === 'songListDroppable') {
      let songListItemId = result.draggableId;
      const item = this.state.songList.song_list_items.find(e => e.id === songListItemId);
      newItems.splice(result.source.index, 1);
      newItems.splice(result.destination.index, 0, item);
    } else {
      const songIdRegex = /^draggable(\d+)/;
      const songId=parseInt(result.draggableId.replace(songIdRegex, '$1'));
      const songListItem = {
        song: {
          ...this.props.songs.find(e => e.id === songId),
          team: null, // To fix a problem due to the normalization
          song_params: null, // Idem
        },
      };
      newItems.splice(result.destination.index, 0, songListItem);
    }

    const newSongList = {
      ...this.state.songList,
      song_list_items: this.updatePosition(newItems),
    };

    this.setState({
      songList: newSongList,
    }, () => this.props.updateList(newSongList));
  }

  onItemSelect(songListItem) {
    this.setState({
      selectedItemId: songListItem.id,
    });
  }

  setCollapse(collapse, itemId) {
    this.setState({
      collapse: {
        ...this.state.collapse,
        [itemId]: collapse,
      },
    });
  }

  // To change a value of the songParams object linked to the song and the user
  // Generic function -> can be used for every type of settings
  onSongSettingsChange(field, value, itemId, request = true) {
    const songListItem = this.state.songList.song_list_items.find(e => e.id === itemId);
    let songParams = {
      transpose: 0,
      capo: 0,
    };
    if (songListItem.song_params && songListItem.length > 0) {
      songParams = songListItem.song_params[0];
    } else if (songListItem.song.song_params && songListItem.song.song_params.length > 0) {
      songParams = songListItem.song.song_params[0];
      // If the song_params comes from the song object, we have to delete the id because we want to create a new object
      delete songParams.id; 
    }
    songParams[field] = value;

    this.setState(prevState => ({
      [field+'s']: {
        ...prevState[field+'s'],
        [itemId]: value,
      },
    }));

    if (request) {
      this.props.setSongParams(songParams, songListItem);
    }
  }

  onCapoChange(capo, itemId, request = true) {
    return this.onSongSettingsChange('capo', parseInt(capo), itemId, request);
  }

  // The transpose value is not linked to the user but only to the item
  // so we need to handle the TransposeChange in a different way than for others settings
  onTransposeChange(transpose, itemId, request = true) {
    this.setState((prevState) => {
      const songListItems = prevState.songList.song_list_items;
      const itemIndex = songListItems.findIndex(e => e.id === itemId);
      if (itemIndex < 0) return prevState;
      const item = prevState.songList.song_list_items[itemIndex];
      const nState = {
        ...prevState,
        songList: {
          ...prevState.songList,
          song_list_items: [
            ...songListItems.slice(0, itemIndex),
            {
              ...item,
              transpose: parseInt(transpose),
            },
            ...songListItems.slice(itemIndex+1)
          ],
        },
      };
      return nState;
    }, () => {
      if (request) {
        this.props.updateSongListItem(this.state.songList.song_list_items.find(e => e.id === itemId));
      }
    });
  }

  onSearchChange(search) {
    this.setState({
      search,
    });
  }

  onEditSongList() {
    this.toggleSongListModal();
  }

  toggleSongListModal() {
    this.setState(prevState => ({
      updateSongListModal: !prevState.updateSongListModal,
    }));
  }

  render() {
    const { preview, songList, collapse, capos, transposes, selectedItemId, search } = this.state;
    if (!songList) return null;
    const { songs, deleteSongList } = this.props;
    const { width, height } = this.state.boxSize;
    const fixedBoxStyle = {
      position: "fixed",
      width: width+"px",
      maxHeight: height+"px",
    };
    const droppableStyle = {
      minHeight: height+"px",
      marginBottom: "24px",
      display: "flex",
      flexDirection: "column",
    };
    let selectedSongListItem = null;

    if (preview) {
      selectedSongListItem = songList.song_list_items.find(e => e.id === selectedItemId)
    }
    return (
      <div>
        <DragDropContext onDragEnd={this.onDragEnd}>
          <Row>
            <Col sm="6" >
              <SongListDroppable 
                  onPreview={this.togglePreview} 
                  preview={preview} 
                  onResize={this.onResize} 
                  onDelete={this.onDelete} 
                  onItemSelect={this.onItemSelect}
                  onDeleteSongList={() => deleteSongList(songList)}
                  songList={songList}
                  songListItems={songList.song_list_items} 
                  collapse={collapse}
                  setCollapse={this.setCollapse}
                  onCapoChange={this.onCapoChange}
                  capos={capos}
                  onTransposeChange={this.onTransposeChange}
                  style={droppableStyle}
                  onEditSongList={this.onEditSongList}
                  id="songListDroppable" />
            </Col>
            <Col sm="6" >
              { preview && selectedSongListItem ? 
                <Song transpose={selectedSongListItem.transpose} capo={capos[selectedItemId]} pending={false} song={selectedSongListItem.song} style={fixedBoxStyle}/>
                :
                <SongsDraggable search={search} onSearchChange={this.onSearchChange} id="songsDraggable" style={fixedBoxStyle}/>
              }
            </Col>
          </Row>
        </DragDropContext>
        <FormModal
          Form={UpdateListForm}
          initialValues={{
            ...songList,
            date: moment(songList.date).toDate(),
          }}
          onSave={this.toggleSongListModal}
          title="Modifier la liste"
          isOpen={this.state.updateSongListModal}
          toggle={this.toggleSongListModal}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  songList: getSongListSelect(ownProps.match.params.songListId, state),
  songs: getSelectedTeamSongs(state),
  selectedTeamId: getSelectedTeamId(state),
  songListPending: isPending("FETCH_SONG_LIST", state),
  songListSuccess: isSuccessful("FETCH_SONG_LIST", state),
});

const mapDispatchToProps = (dispatch) => ({
  loadSongList: (songListId) => {
    dispatch(getSongList(songListId));
  },
  updateList: (songList) => {
    dispatch(updateSongList(songList))
  },
  loadSongs: () => {
    dispatch(getSongs());
  },
  deleteSongList: (songList) => {
    dispatch(deleteSongListRequest(songList));
  },
  setSongParams: (songParams, songListItem) => dispatch(setSongParamsRequest(songParams, null, songListItem)),
  updateSongListItem: (songListItem) => {
    dispatch(updateSongListItemRequest(songListItem));
  }
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ShowSongList));