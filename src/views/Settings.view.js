import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Card, CardBody, CardHeader, Row, Col } from 'reactstrap';
import TeamList from '../components/Settings/TeamList';

import { getMyTeams } from '../reducers/rootSelectors';
class Settings extends Component {

  render() {
    const { teams } = this.props;
    return (
      <div className="animated fadeIn">
        <Row>
          <Col lg="12">
            <Card>
              <CardHeader>
                Paramètres
              </CardHeader>
              <CardBody>
                <TeamList teams={teams} />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  teams: getMyTeams(state),
});

const mapDispatchToProps = (state) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(Settings);