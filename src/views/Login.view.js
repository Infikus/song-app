import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loginRequest } from '../actions/users.action';
import FormModal from '../components/FormModal/FormModal';
import {
  Container, Row, Col, CardGroup, Card, CardBody, Button, Alert
} from 'reactstrap';

import LoginForm from '../forms/login.form';
import RegisterForm from '../containers/User/RegisterForm'

import backgroundImg from './img/loginBackground.jpg';
import modalStyle from './css/centeredModal.css';
import { isRegisterSuccess, isSuccessful, isError } from '../reducers/rootSelectors';
import { isPending } from '../reducers/rootSelectors';
import ErrorMessage from '../components/Common/ErrorMessage';

class Lists extends Component {

  constructor(props) {
    super(props);
    this.state = {
      registerModal: false,
      registrationSucceed: false,
    }

    this.onSubmit = this.onSubmit.bind(this);
    this.toggleRegisterModal = this.toggleRegisterModal.bind(this);
  }

  toggleRegisterModal() {
    this.setState({
      registerModal: !this.state.registerModal,
      registrationSucceed: false, // on reset le message s'il existe
    });
  }

  onSubmit(user) {
    this.props.login(user);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.registerSuccess && !this.props.registerSuccess && this.state.registerModal) {
      this.setState({
        registerModal: false,
        registrationSucceed: true,
      })
    }
  }

  render() {
    //const {error} = this.props.users;
    const error = "";
    return (
      <div className="app flex-row align-items-center" style={{
        backgroundImage: "url("+backgroundImg+")",
        backgroundRepeat: "no-repeat", 
        backgroundSize: "cover",
        backgroundPosition: "center",
        }}>
        <Container>
        <Row className="justify-content-center">
          <Col lg="8" md="10">
          <Row>
            <Card className="p-4 col-lg-7 col-md-6 col-sm-12">
              <CardBody>
                <ErrorMessage show={this.props.registerSuccess} color="success" timeout={3}>Inscription confirmée</ErrorMessage>
                <ErrorMessage show={!!this.props.loginError}>{this.props.loginError}</ErrorMessage>
                <h1>Connexion</h1>
                <p className="text-muted">Connectez-vous pour accéder à votre compte.</p>
                {error ? (
                  <div className="mb-3" style={{
                    color: "red",
                    textAlign: "center",
                    fontSize: "1em",
                  }}>{error}</div>
                ) : null}
                <LoginForm loginPending={this.props.loginPending} onSubmit={this.onSubmit}/>
              </CardBody>
            </Card>
            <Card className="text-white bg-primary py-5 col-lg-5 col-md-6 col-sm-12" style={{ opacity: "0.95" }}>
              <CardBody className="text-center">
                <div>
                <h2>Inscription</h2>
                <p>Toujours pas inscrit ? Clique sur le bouton ci dessous pour avoir accès à l'outil.</p>
                <Button onClick={this.toggleRegisterModal} color="primary" className="mt-3" active>S'inscrire maintenant !</Button>
                </div>
              </CardBody>
            </Card>
          </Row>
          </Col>
        </Row>
        <FormModal 
          className="vertical-align-center"
          Form={RegisterForm}
          title="Inscription"
          modalSize="md"
          isOpen={this.state.registerModal}
          toggle={this.toggleRegisterModal}
          />
        </Container>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
  login: (user) =>  {
    dispatch(loginRequest(user))
  },
});

const mapStateToProps = (state) => ({
  registerSuccess: isSuccessful("REGISTER", state),
  loginError: isError("LOGIN", state),
  loginPending: isPending("LOGIN", state),
});

export default connect(mapStateToProps, mapDispatchToProps)(Lists);
