import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Card, Col, Row, CardBody, CardTitle, CardHeader, Button, Modal, ModalHeader, ModalBody, ModalFooter
} from 'reactstrap';
import SongList from '../components/SongList/SongList';
import CreateSongForm from '../containers/Song/CreateSongForm';
import FormModal from '../components/FormModal/FormModal';
import ImportSongModal from '../components/ImportSong/ImportSongModal';

import { getAllSongs, isSongPending, getSelectedTeamId, getMe, isPending, isSuccessful, isError, getSelectedTeamSongsPaginate } from '../reducers/rootSelectors';
import { getSongs, postSong } from '../actions/songs.action';
import SongListPagination from '../components/SongList/SongListPagination';
import SongListFilters from '../components/SongList/SongListFilters';
import { FILTER_TITLE_ASC } from '../reducers/app/me.reducer';

class Songs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newSongModal: false,
      importSongModal: false,
      page: 0,
      pageSize: 10,
      search: '',
      filter: FILTER_TITLE_ASC,
      songs: {
        count: 0,
        data: [],
      },
    }

    this.toggleNewSongModal = this.toggleNewSongModal.bind(this);
    this.toggleImportSongModal = this.toggleImportSongModal.bind(this);
    this.onPageChange = this.onPageChange.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
    this.onFilterChange = this.onFilterChange.bind(this);
    this.checkPage = this.checkPage.bind(this);
  }

  toggleNewSongModal() {
    this.setState({
      newSongModal: !this.state.newSongModal,
    });
  }

  toggleImportSongModal() {
    this.setState({
      importSongModal: !this.state.importSongModal,
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.addSongSuccess && !this.props.addSongSuccess && this.state.newSongModal) {
      this.setState({
        newSongModal: false,
      });
    }
    // The selected team has changed
    if (nextProps.selectedTeamId !== this.props.selectedTeamId) {
      this.props.loadSongs();
    }

    this.setState(prevState => ({
      songs: nextProps.getSongs(prevState.page, prevState.pageSize, prevState.search, prevState.filter),
    }), () => this.checkPage());
  }

  checkPage() {
    const { page, pageSize, songs } = this.state;
    const maxPage = Math.floor(songs.count/pageSize);
    if (page !== 0 && page > maxPage) {
      this.onPageChange(maxPage);
    }
  }

  onPageChange(page) {
    this.setState(prevState => ({
      songs: this.props.getSongs(page, prevState.pageSize, prevState.search, prevState.filter),
      page,
    }), () => this.checkPage());

  }

  onSearchChange(search) {
    this.setState(prevState => ({
      songs: this.props.getSongs(prevState.page, prevState.pageSize, search, prevState.filter),
      search,
    }), () => this.checkPage());
  }

  onFilterChange(filter) {
    this.setState(prevState => ({
      songs: this.props.getSongs(prevState.page, prevState.pageSize, prevState.search, filter),
      filter,
    }));
  }

  render() {
    return (
    <div className="animated fadeIn">
      <Row>
      <Col lg="12">
        <Card>
        <CardHeader>
          Liste des chants
          <div className="float-right">
            <Button onClick={this.toggleImportSongModal} color="primary" size="sm">Importer</Button>{' '}
            <Button onClick={this.toggleNewSongModal} color="primary" size="sm">Nouveau chant</Button>
          </div>
        </CardHeader>
        <CardBody>
          <SongListFilters
            onSearchChange={this.onSearchChange}
            search={this.state.search}
          />
          <SongList
            songs={this.state.songs.data}
            onFilterChange={this.onFilterChange}
            filter={this.state.filter}
            loadData={this.props.loadSongs}
          />
          <SongListPagination
            pageCount={Math.ceil(this.state.songs.count/this.state.pageSize)}
            onPageChange={this.onPageChange}
            page={this.state.page}
          />
        </CardBody>
        </Card>
      </Col>
      </Row>
      <FormModal 
        Form={CreateSongForm}
        title="Nouveau chant"
        isOpen={this.state.newSongModal}
        toggle={this.toggleNewSongModal}
      />
      <ImportSongModal
        isOpen={this.state.importSongModal}
        toggle={this.toggleImportSongModal}
        saveSong={this.props.saveSong}
        pending={this.props.addSongPending}
        success={this.props.addSongSuccess}
        failure={this.props.addSongError}
      />
    </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
  loadSongs: () =>  {
    dispatch(getSongs())
  },
  saveSong: (song) => {
    dispatch(postSong(song));
  }
});

const mapStateToProps = (state) => ({
  addSongPending: isPending("POST_SONG", state),
  addSongSuccess: isSuccessful("POST_SONG", state),
  addSongError: isError("POST_SONG", state),
  selectedTeamId: getSelectedTeamId(state),
  getSongs: (page, pageSize, search, filter) => getSelectedTeamSongsPaginate(page, pageSize, search, filter, state),
  me: getMe(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(Songs);
