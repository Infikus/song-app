import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Nav, NavItem, NavLink, TabContent, TabPane, Col, Row } from 'reactstrap';
import classnames from 'classnames';

import GeneralTeamForm from '../containers/Settings/GeneralTeamForm';
import AddMemberForm from '../containers/Settings/AddMemberForm';

import { fetchTeamRequest } from '../actions/teams.action';
import { getTeam, getMyRoleInTeam, isSuccessful, isError } from '../reducers/rootSelectors';
import TeamMembersList from '../components/Settings/TeamMembersList';
import { updateTeamMemberRequest, deleteTeamMemberRequest } from '../actions/teamMembers.action';
import ErrorMessage from '../components/Common/ErrorMessage';

class TeamSettings extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      activeTab: '1'
    };

    this.toggle = this.toggle.bind(this);
    this.onRoleChange = this.onRoleChange.bind(this);
  }

  componentDidMount() {
    // Load the team
    this.props.getTeam(this.props.match.params.teamId);
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  onRoleChange(teamMemberId, role) {
    const teamMember = this.props.team.team_members.find(tm => tm.id === teamMemberId);
    this.props.updateTeamMember({
      ...teamMember,
      role: role,
    });
  }

  render() {
    const { team, role } = this.props;
    if (!team) {
      return (
        <p>Loading...</p>
      )
    }
    return (
      <div>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '1' })}
              onClick={() => { this.toggle('1'); }}
            >
              General
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '2' })}
              onClick={() => { this.toggle('2'); }}
            >
              Membres
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <Row>
              <Col sm="12">
                  <GeneralTeamForm
                    team={team}
                    canEdit={role == 0}
                  />
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="2">
            <Row>
              <Col sm="12">
                <ErrorMessage show={this.props.addMemberSuccess} timeout={3} color={"success"}>Membre ajouté</ErrorMessage>
                <ErrorMessage show={this.props.updateMemberSuccess} timeout={3} color={"success"}>Modification enregistrée</ErrorMessage>
                <ErrorMessage show={this.props.deleteMemberSuccess} timeout={3} color={"success"}>Membre retiré du groupe</ErrorMessage>
                <ErrorMessage show={!!this.props.addMemberError}>{this.props.addMemberError}</ErrorMessage>
                <ErrorMessage show={!!this.props.updateMemberError}>{this.props.updateMemberError}</ErrorMessage>
                <ErrorMessage show={!!this.props.deleteMemberError}>{this.props.deleteMemberError}</ErrorMessage>

                {role == 0 && <AddMemberForm team={team}/>}
                <TeamMembersList
                  team={team}
                  canEdit={role == 0}
                  deleteTeamMember={this.props.deleteTeamMember}
                  onRoleChange={this.onRoleChange}
                />
              </Col>
            </Row>
          </TabPane>
        </TabContent>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  team: getTeam(ownProps.match.params.teamId, state),
  role: getMyRoleInTeam(ownProps.match.params.teamId, state),

  addMemberSuccess: isSuccessful("ADD_TEAM_MEMBER", state),
  addMemberError: isError("ADD_TEAM_MEMBER", state),
  updateMemberSuccess: isSuccessful("UPDATE_TEAM_MEMBER", state),
  updateMemberError: isError("UPDATE_TEAM_MEMBER", state),
  deleteMemberSuccess: isSuccessful("DELETE_TEAM_MEMBER", state),
  deleteMemberError: isError("DELETE_TEAM_MEMBER", state),
});

const mapDispatchToProps = (dispatch) => ({
  getTeam: (teamId) => dispatch(fetchTeamRequest(teamId)),
  updateTeamMember: (teamMember) => dispatch(updateTeamMemberRequest(teamMember)),
  deleteTeamMember: (teamMember) => dispatch(deleteTeamMemberRequest(teamMember)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TeamSettings);