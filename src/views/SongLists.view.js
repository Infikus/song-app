import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getSongLists } from '../actions/songLists.action';
import {
  Card, Col, Row, CardBody, CardTitle, CardHeader, Button
} from 'reactstrap';
import FormModal from '../components/FormModal/FormModal';

import SongListList from '../components/SongListList/SongListList';
import CreateListForm from '../containers/Song/CreateListForm';
import { getSelectedTeamId, getSelectedTeamSongLists, isPending, isSuccessful } from '../reducers/rootSelectors';

class SongLists extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newListModal: false,
        }

        this.toggleNewListModal = this.toggleNewListModal.bind(this);
    }

    toggleNewListModal() {
        this.setState({
            newListModal: !this.state.newListModal,
        });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.addSongListSuccess && !this.props.addSongListSuccess && this.state.newListModal) {
            this.setState({
                newListModal: false,
            });
        }
        // The selected team has changed
        if (nextProps.selectedTeamId !== this.props.selectedTeamId) {
            this.props.loadSongLists();
        }
    }

    render() {
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col lg="12">
                        <Card>
                        <CardHeader>
                            Listes
                            <Button onClick={this.toggleNewListModal} color="primary" size="sm" className="float-right">Nouvelle liste</Button>
                        </CardHeader>
                        <CardBody>
                            <SongListList songLists={this.props.songLists} loadData={this.props.loadSongLists}/>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>
                <FormModal 
                    title={"Nouvelle liste"}
                    Form={CreateListForm}
                    isOpen={this.state.newListModal}
                    toggle={this.toggleNewListModal}
                    />
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    loadSongLists: ()=>  {
        dispatch(getSongLists())
    },
});

const mapStateToProps = (state) => ({
    addSongListPending: isPending("POST_SONG_LIST", state),
    addSongListSuccess: isSuccessful("POST_SONG_LIST", state),
    songLists: getSelectedTeamSongLists(state),
    selectedTeamId: getSelectedTeamId(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(SongLists);
