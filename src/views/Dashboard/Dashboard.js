import React, { Component } from 'react';
import {
  Card, Col, Row, CardBody, CardTitle, CardHeader
} from 'reactstrap';
import CreateSongForm from '../../containers/Song/CreateSongForm';

class Dashboard extends Component {

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col lg="12">
            <Card>
              <CardHeader>
                Nouveau chant
              </CardHeader>
              <CardBody>
                <CreateSongForm />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Dashboard;
