import { combineReducers } from 'redux';

import usersEntities from './entities/users.reducer';
import songsEntities from './entities/songs.reducer';
import songParamsEntities from './entities/songParams.reducer';
import songListsEntities from './entities/songLists.reducer';
import songListItemsEntities from './entities/songListItems.reducer';
import teamsEntities from './entities/teams.reducer';
import teamMembersEntities from './entities/teamMembers.reducer';

import meApp from './app/me.reducer';
import songsApp from './app/songs.reducer';
import pendingApp from './app/pending.reducer';

import { routerReducer } from 'react-router-redux';


const entitiesReducer = combineReducers({
    users: usersEntities,
    songs: songsEntities,
    song_params: songParamsEntities,
    song_lists: songListsEntities,
    song_list_items: songListItemsEntities,
    teams: teamsEntities,
    team_members: teamMembersEntities,
});

const appReducer = combineReducers({
    me: meApp,
    songs: songsApp,
    pending: pendingApp,
})

const rootReducer = combineReducers({
    entities: entitiesReducer,
    app: appReducer,
    routing: routerReducer,
});

export default rootReducer;