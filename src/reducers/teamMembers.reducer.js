import * as actions from '../actions/teamMembers.action';

function teamMembers(state, action) {
    switch (action.type) {
        case actions.ADD_TEAM_MEMBER_REQUEST:
            return Object.assign({}, state, {
                pending: true,
                error: null,
            });
        case actions.ADD_TEAM_MEMBER_FAILURE:
            return Object.assign({}, state, {
                pending: false,
                error: action.message,
            });
        case actions.ADD_TEAM_MEMBER_SUCCESS:
            return Object.assign({}, state, {
                pending: false,
                me: Object.assign({}, state.me, {
                    teams: [...state.me.teams, action.teamMember],
                }),
            });
    }

    return state;
} 

export default teamMembers;