import * as actions from '../actions/users.action';
import { setSelectedTeam } from '../actions/users.action';
import { select } from 'redux-saga/effects';
import teamMembersReducer from './teamMembers.reducer';

function getInitialSelectedTeam() {
    const team = localStorage.getItem("selectedTeam");
    try {
        return team ? JSON.parse(team) : null;
    } catch (e) {
        return null;
    }
}

function setSelectedTeamLocal(team) {
    return localStorage.setItem('selectedTeam', JSON.stringify(team));
}

function updateSelectedTeam(state, user) {
    if (!state.selectedTeam) {
        const team = Object.assign({}, user.last_team.team, {
            teamMemberId: user.last_team.id,
            role: user.last_team.role,
        });
        setSelectedTeamLocal(team);
        return team;
    }
    return state.selectedTeam;
}

const initialState = {
    registrationSuccess: false,

    mePending: false,
    me: null,

    error: null,
    pending: false,

    selectedTeam: getInitialSelectedTeam(),
};

function users(state=initialState, action) {
    switch(action.type) {
        case actions.LOGIN_REQUEST:
            return Object.assign({}, state, {
                pending: true,
                error: null,
            });
        case actions.LOGIN_SUCCESS:
            return Object.assign({}, state, {
                pending: false,
                error: null,
                selectedTeam: null,
            });
        case actions.LOGIN_FAILURE:
            return Object.assign({}, state, {
                pending: false,
                error: action.message,
            });
        
        case actions.REGISTER_REQUEST:
            return Object.assign({}, state, {
                pending: true,
                error: null,
                registrationSuccess: false,
            });
        case actions.REGISTER_SUCCESS:
            return Object.assign({}, state, {
                pending: false,
                error: null,
                registrationSuccess: true,
            });
        case actions.REGISTER_FAILURE:
            return Object.assign({}, state, {
                pending: false,
                error: action.message,
                registrationSuccess: false,
            });
        case actions.REGISTER_RESET:
            return Object.assign({}, state, {
                pending: false,
                error: null,
                registrationSuccess: false,
            });
        
        case actions.ME_REQUEST:
            return Object.assign({}, state, {
                pending: true,
                error: null,
                me: null,
            });
        case actions.ME_SUCCESS:
            return Object.assign({}, state, {
                pending: false,
                error: null,
                me: action.user,
                selectedTeam: updateSelectedTeam(state, action.user),
            });
        case actions.ME_FAILURE:
            return Object.assign({}, state, {
                pending: false,
                error: action.message,
                me: null,
            });

        case actions.POST_TEAM_REQUEST:
            return Object.assign({}, state, {
                pending: true,
                error: null,
            });
        case actions.POST_TEAM_SUCCESS:
            const newTeam = formatTeam(action.team);
            setSelectedTeamLocal(newTeam);
            return Object.assign({}, state, {
                pending: false,
                error: null,
                // TODO : add action.team
                me: Object.assign({}, state.me, {
                    teams: [...state.me.teams, action.team],
                }),
                selectedTeam: newTeam
            });
        case actions.POST_TEAM_FAILURE:
            return Object.assign({}, state, {
                pending: false,
                error: action.message,
            });

        case actions.UPDATE_TEAM_REQUEST:
            return Object.assign({}, state, {
                pending: true,
                error: null,
            });
        case actions.UPDATE_TEAM_SUCCESS: 
            const oldTeamMember = state.me.teams.find(t => t.team.id == action.team.id);
            const newTeamMember = Object.assign({}, oldTeamMember, {
                team: action.team,
            });
            const updatedTeam = formatTeam(newTeamMember);
            setSelectedTeamLocal(updatedTeam);
            return Object.assign({}, state, {
                pending: false,
                selectedTeam: state.selectedTeam.id === updatedTeam.id ? updatedTeam : state.selectedTeam,
                me: Object.assign({}, state.me, {
                    teams: state.me.teams.map(g => g.id === newTeamMember.id ? newTeamMember :  g)
                }),
            })
        case actions.UPDATE_TEAM_FAILURE:
            return Object.assign({}, state, {
                pending: false,
                error: action.message,
            })
        
        case actions.DELETE_TEAM_REQUEST:
            return Object.assign({}, state, {
                pending: true,
                error: null,
            })
        case actions.DELETE_TEAM_SUCCESS:
            const deletedTeamIndex = state.me.teams.findIndex(e => e.team.id === action.team.id);
            const privateTeamIndex = state.me.teams.findIndex(e => e.team.private);
            let selectedTeam = state.selectedTeam;
            if (selectedTeam.id === action.team.id) {
                selectedTeam = formatTeam(state.me.teams[privateTeamIndex]);
            }
            setSelectedTeamLocal(selectedTeam);
            return Object.assign({}, state, {
                pending:false,
                me: Object.assign({}, state.me, {
                    teams: [...state.me.teams.slice(0, deletedTeamIndex), ...state.me.teams.slice(deletedTeamIndex+1)]
                }),
                selectedTeam,
            })
        case actions.DELETE_TEAM_FAILURE:
            return Object.assign({}, state, {
                pending: false,
                error: action.message,
            })

        case actions.SET_SELECTED_TEAM:
            setSelectedTeamLocal(action.team);
            return Object.assign({}, state, {
                selectedTeam: action.team,
            });
    }
    return teamMembersReducer(state, action);
}

export function getSelectedTeam(state) {
    return state.users.selectedTeam;
}

export function getMyTeams(state) {
    if (state.users.me)
        return state.users.me.teams.map(userTeam => {
            return formatTeam(userTeam);
        });
    return [];
}

function formatTeam(team) {
    return Object.assign({}, team.team, {
        role: team.role,
        teamMemberId: team.id,
    })
}

export default users;
