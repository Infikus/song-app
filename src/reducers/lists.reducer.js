import * as actions from '../actions/lists.action';

const initialState = {
    list: {},
    current: {
        list: null,
        pending: false,
        error: null,
    },
    pending: false,
    error: null,
};

function lists(state=initialState, action) {
    switch (action.type) {
        case actions.FETCH_LISTS_REQUEST:
            return Object.assign({}, state, {
                pending: true
            });
        case actions.FETCH_LISTS_SUCCESS:
            return Object.assign({}, state, {
                pending: false,
                list: action.lists.reduce((acc, e) => {
                    acc[e.id] = e;
                    return acc;
                }, {}),
                error: null
            });
        case actions.FETCH_LISTS_FAILURE:
            return Object.assign({}, state, {
                pending: false,
                error: action.error
            });
        
        case actions.FETCH_LIST_REQUEST:
            /* Set current.pending to true */
            return Object.assign({}, state, { 
                    current: Object.assign({}, state.current, {pending: true, list: null, error: null})
                }
            );
        case actions.FETCH_LIST_SUCCESS:
            return Object.assign({}, state, {
                current: {
                    pending: false,
                    list: action.list,
                    error: null
                }
            });
        case actions.FETCH_LIST_FAILURE:
            return Object.assign({}, state, {
                current: {
                    list: null,
                    pending: false,
                    error: action.error
                }
            });

        case actions.POST_LIST_REQUEST:
            return Object.assign({}, state, {
                pending: true
            });
        case actions.POST_LIST_SUCCESS:
            let newList = {};
            newList[action.list.id] = action.list;
            return Object.assign({}, state, {
                pending: false,
                list: Object.assign({}, state.list, newList)
            });
        case actions.POST_LIST_FAILURE:
            return Object.assign({}, state, {
                pending: false
            });
        
        case actions.UPDATE_LIST_REQUEST:
            return Object.assign({}, state, {
                pending: true
            });
        case actions.UPDATE_LIST_SUCCESS:
            let list = {};
            list[action.list.id] = action.list;
            let currentList = state.current.list;
            if (currentList.id === action.list.id) {
                currentList = action.list;
            }
            return Object.assign({}, state, {
                pending: false,
                list: Object.assign({}, state.list, list),
                current: {
                    list: currentList,
                },
            });
        case actions.UPDATE_LIST_FAILURE:
            return Object.assign({}, state, {
                pending: false
            });
        default:
            return state;
    }
}

export default lists;