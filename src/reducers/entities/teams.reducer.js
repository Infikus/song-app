import * as songActions from '../../actions/songs.action';
import * as actions from '../../actions/teams.action';
import * as userActions from '../../actions/users.action';
import * as songListActions from '../../actions/songLists.action';
import * as teamMemberActions from '../../actions/teamMembers.action';
import { denormalize } from 'normalizr';
import { songSchema, teamSchema, songsSchema } from '../../sagas/api/schemas';

const initialState = {};

function teams(state=initialState, action) {
  switch (action.type) {

    case userActions.ME_SUCCESS:
    case actions.POST_TEAM_SUCCESS:
    case actions.FETCH_TEAM_SUCCESS:
      return {
        ...state,
        ...action.payload.entities.teams
      };
    case actions.DELETE_TEAM_SUCCESS:
      return Object.keys(state).reduce((acc, teamId) => {
        if (teamId == action.payload.teamId)
          return acc;
        return {
          ...acc,
          [teamId]: state[teamId],
        };
      }, {});
    case songActions.POST_SONG_SUCCESS:
      const { songId } = action.payload;
      let teamId = Object.keys(action.payload.entities.teams)[0];
      return {
        ...state,
        [teamId]: {
          ...state[teamId],
          songs:  [...state[teamId].songs, songId]
        },
      };
    case songActions.FETCH_SONGS_SUCCESS:
      teamId = Object.keys(action.payload.entities.teams)[0];
      return {
        ...state,
        [teamId]: {
          ...state[teamId],
          songs: action.payload.songsId,
        },
      };
    case songListActions.FETCH_SONG_LISTS_SUCCESS:
      teamId = Object.keys(action.payload.entities.teams)[0];
      return {
        ...state,
        [teamId]: {
          ...state[teamId],
          song_lists: action.payload.songListsId,
        },
      };
    case songListActions.POST_SONG_LIST_SUCCESS:
      console.log(action);
      teamId = Object.keys(action.payload.entities.teams)[0];
      return {
        ...state,
        [teamId]: {
          ...state[teamId],
          song_lists: [...state[teamId].song_lists, action.payload.songListId],
        },
      };
    case teamMemberActions.ADD_TEAM_MEMBER_SUCCESS:
      let teamMemberId = action.payload.teamMemberId;
      teamId = Object.keys(action.payload.entities.teams)[0];
      return {
        ...state,
        [teamId]: {
          ...state[teamId],
          team_members: [...state[teamId].team_members, teamMemberId],
        },
      };
    case teamMemberActions.DELETE_TEAM_MEMBER_SUCCESS:
      console.log(action.payload);
      teamId = Object.keys(action.payload.entities.teams)[0];
      teamMemberId = state[teamId].team_members.findIndex(teamMember => teamMember == action.payload.teamMemberId);
      return {
        ...state,
        [teamId]: {
          ...state[teamId],
          team_members: [
            ...state[teamId].team_members.slice(0, teamMemberId),
            ...state[teamId].team_members.slice(teamMemberId+1),
          ],
        },
      };

    case userActions.LOGOUT :
      return initialState;

    default:
      return state;
  }
}

export const getTeam = (id, state) => {
  return denormalize(id, teamSchema, state);
}
export const getTeamSongs = (teamId, state) => {
  //return denormalize(state.teams[teamId].songs ,songsSchema, state);
  if (!state.teams[teamId] || !state.teams[teamId].songs)
    return [];
  return state.teams[teamId].songs.map(id => {
    return state.songs[id];
  })
}
export const getTeamSongLists = (teamId, state) => {
  if (!state.teams[teamId] || !state.teams[teamId].song_lists)
    return [];
  return state.teams[teamId].song_lists.map(id => {
    return state.song_lists[id];
  })
}
export const getAllTeams = (state) => {
  return Object.keys(state.teams)
    .map(id => state.teams[id])
    .sort((a, b) => a.name.toLowerCase() > b.name.toLowerCase());
}

export default teams;