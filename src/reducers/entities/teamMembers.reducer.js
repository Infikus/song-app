import * as songActions from '../../actions/songs.action';
import * as teamActions from '../../actions/teams.action';
import * as userActions from '../../actions/users.action';
import * as actions from '../../actions/teamMembers.action';
import { denormalize } from 'normalizr';
import { teamMemberSchema } from '../../sagas/api/schemas';

export const roleToString = (role) => {
  switch (role) {
    case 0: return "Responsable";
    case 1: return "Membre";
    default: return "Indéfini";
  }
}

const initialState = {};

function teamMembers(state=initialState, action) {
  switch (action.type) {
    case userActions.ME_SUCCESS:
    case teamActions.POST_TEAM_SUCCESS:
    case teamActions.FETCH_TEAM_SUCCESS:
    case actions.ADD_TEAM_MEMBER_SUCCESS:
    case actions.UPDATE_TEAM_MEMBER_SUCCESS:
      let {entities} = action.payload;
      return {
        ...state,
        ...entities.team_members,
      };

    case actions.DELETE_TEAM_MEMBER_SUCCESS:
      return Object.keys(state).reduce((acc, id) => {
        if (id == action.payload.teamMemberId) {
          return acc;
        }
        return {
          ...acc,
          [id]: state[id],
        };
      }, {});

    case teamActions.DELETE_TEAM_SUCCESS:
      return Object.keys(state).reduce((acc, id) => {
        if (state[id].team == action.payload.teamId)
          return acc;
        else return {
          ...acc,
          [id]: state[id],
        };
      }, {});

    case userActions.LOGOUT :
      return initialState;

    default:
      return state;
  }
}

export const getTeamMember = (id, state) => {
  return denormalize(id, teamMemberSchema, state);
}

export default teamMembers;