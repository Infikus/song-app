import * as actions from '../../actions/songs.action';
import * as songListActions from '../../actions/songLists.action';
import { denormalize } from 'normalizr';
import { songSchema } from '../../sagas/api/schemas';
import { LOGOUT } from '../../actions/users.action';


function mergeState(state, songs) {
  if (songs) {
    return Object.keys(songs).reduce((stateAcc, songId) => {
      return {
        ...stateAcc,
        [songId]: {
          ...stateAcc[songId],
          ...songs[songId],
        },
      };
    }, state);
  }
  return state;
}

const initialState = {};

function songs(state=initialState, action) {
  switch (action.type) {

    case actions.FETCH_SONGS_SUCCESS:
    case actions.POST_SONG_SUCCESS:
    case actions.UPDATE_SONG_SUCCESS:
    case actions.FETCH_SONG_SUCCESS:
    case songListActions.FETCH_SONG_LIST_SUCCESS:
    case songListActions.FETCH_SONG_LISTS_SUCCESS:
    case songListActions.POST_SONG_LIST_SUCCESS:
    case songListActions.UPDATE_SONG_LIST_SUCCESS:
      const {entities} = action.payload;
      return mergeState(state, entities.songs);
      /**
      return {
        ...state,
        ...entities.songs
      }; */

    case actions.DELETE_SONG_SUCCESS: {
      const { songId } = action.payload;
      if (!state[songId]) return state;
      return Object.keys(state)
        .filter(k => parseInt(k) !== songId)
        .reduce((acc, id) => ({
          ...acc,
          [id]: state[id],
        }), {});
    }
      
    case LOGOUT :
      return initialState;

    default:
      return state;
  }
}

export const getSong = (id, state) => {
  return denormalize(id, songSchema, state);
}

// Return all songs ordered by name
export const getAllSongs = (state) => {
  return Object.keys(state.songs)
    .map(id => state.songs[id])
    .sort((a, b) => a.title.toLowerCase() > b.title.toLowerCase());
}

export default songs;