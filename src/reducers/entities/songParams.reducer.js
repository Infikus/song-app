import * as actions from '../../actions/songParams.action';
import * as songsActions from '../../actions/songs.action';
import * as songListsActions from '../../actions/songLists.action';
import { LOGOUT } from '../../actions/users.action';

const initialState = {};

function songParams(state = initialState, action) {
  switch (action.type) {
    case songsActions.FETCH_SONG_SUCCESS:
    case songsActions.FETCH_SONGS_SUCCESS:
    case songListsActions.FETCH_SONG_LIST_SUCCESS:
    case songListsActions.FETCH_SONG_LISTS_SUCCESS:
    case actions.SET_SONG_PARAMS_SUCCESS:
      return {
        ...state,
        ...action.payload.entities.song_params,
      };

    case LOGOUT :
      return initialState;

    default:
      return state;
  }
}

export default songParams;