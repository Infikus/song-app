import * as actions from '../../actions/songListItems.action';
import * as songListActions from '../../actions/songLists.action';
import * as songParamsActions from '../../actions/songParams.action';
import { denormalize } from 'normalizr';
import { songSchema } from '../../sagas/api/schemas';
import { LOGOUT } from '../../actions/users.action';

const initialState = {};

function songListItems(state=initialState, action) {
  switch (action.type) {

  case actions.UPDATE_SONG_LIST_ITEM_SUCCESS: {
    const itemId = action.payload.songListItemId;
    const item = action.payload.entities.song_list_items[itemId];
    const transpose = item.transpose ? item.transpose : 0;
    return {
      ...state,
      [itemId]: {
        ...state[itemId],
        transpose,
      },
    };
  }

  case songListActions.FETCH_SONG_LISTS_SUCCESS:
  case songListActions.POST_SONG_LIST_SUCCESS:
  case songListActions.UPDATE_SONG_LIST_SUCCESS:
  case songListActions.FETCH_SONG_LIST_SUCCESS: {
    const {entities} = action.payload;
    return {
    ...state,
    ...entities.song_list_items
    };
  }

  case songParamsActions.SET_SONG_PARAMS_SUCCESS: {
    const { entities, songParamsId } = action.payload;
    if (entities.song_list_items) {
      const itemId = Object.keys(entities.song_list_items)[0];
      const newSongParams = [songParamsId]; // There can be only one songParam per user
      return {
        ...state,
        [itemId] : {
          ...state[itemId],
          song_params: newSongParams,
        },
      };
    }
    return state;
  }

  case LOGOUT :
    return initialState;

  default:
    return state;
  }
}

export default songListItems;