import * as actions from '../../actions/songLists.action';
import { denormalize } from 'normalizr';
import { songSchema, songListSchema } from '../../sagas/api/schemas';
import { LOGOUT } from '../../actions/users.action';

const initialState = {};

function songLists(state=initialState, action) {
  switch (action.type) {

  case actions.FETCH_SONG_LISTS_SUCCESS:
  case actions.POST_SONG_LIST_SUCCESS:
  case actions.UPDATE_SONG_LIST_SUCCESS:
  case actions.FETCH_SONG_LIST_SUCCESS:
    const {entities} = action.payload;
    return {
    ...state,
    ...entities.song_lists
    };

  case actions.DELETE_SONG_LIST_SUCCESS: {
      const { songListId } = action.payload;
      if (!state[songListId]) return state;
      return Object.keys(state)
        .filter(k => parseInt(k) !== songListId)
        .reduce((acc, id) => ({
          ...acc,
          [id]: state[id],
        }), {});
    }

  case LOGOUT :
    return initialState;

  default:
    return state;
  }
}

export const getSongList = (id, state, denorm=true) => {
  if (denorm) {
     const data = denormalize(id, songListSchema, state);
     if (data && data.song_list_items) {
      return {
        ...data,
        song_list_items: data.song_list_items.sort((a, b) => a.position < b.position ? -1 : 1),
      }
    }
    return data;
  }
  return state.song_lists[id];
}
// Return all the songs in a list, ordered by position
export const getSongsOfSongList = (id, state) => {
  const itemsId = state.song_lists[id] ? state.song_lists[id].song_list_items : [];
  const res = itemsId
    .map(id => {
      const songListItem = state.song_list_items[id];
      return {
        ...songListItem,
        song: {
          ...state.songs[songListItem.song],
          songListItemId: id,
        },
      };
    })
    .sort((item1, item2) => item1.position > item2.position)
    .map(item => item.song);
  return res;
}

export default songLists;