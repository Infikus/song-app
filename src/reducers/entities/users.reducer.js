import * as songActions from '../../actions/songs.action';
import * as actions from '../../actions/teams.action';
import * as userActions from '../../actions/users.action';
import * as teamActions from '../../actions/teams.action';
import * as teamMemberActions from '../../actions/teamMembers.action';
import { denormalize } from 'normalizr';
import { userSchema } from '../../sagas/api/schemas';

const initialState = {};

function mergeArrayUniq(array1, array2) {
  return array1.reduce((acc, val) => {
    if (acc.findIndex((e) => e == val) === -1)
      return [...acc, val];
    return acc;
  }, array2);
}

function mergeUsers(state, entities) {
  return Object.keys(entities.users).reduce((acc, id) => {
    const user = entities.users[id];
    if (!state[id]) 
      return {
        ...acc,
        [id]: user,
      };
    return {
      ...acc,
      [id]: {
        ...state[id],
        ...user,
        team_members: mergeArrayUniq(user.team_members, state[id].team_members),
      },
    };
  }, state);
}

function isInArray(value, array) {
  return array.indexOf(value) > -1;
}

function users(state=initialState, action) {
  switch (action.type) {

    case userActions.ME_SUCCESS:
      const {entities} = action.payload;
      return {
        ...state,
        ...entities.users,
      };
    
    case teamActions.FETCH_TEAM_SUCCESS:
    case teamMemberActions.ADD_TEAM_MEMBER_SUCCESS:
      return mergeUsers(state, action.payload.entities);

    case teamActions.POST_TEAM_SUCCESS:
      let userId = Object.keys(action.payload.entities.users)[0]; // There is only one user in a new group
      let teamMemberId = Object.keys(action.payload.entities.team_members)[0];
      return {
        ...state,
        [userId]: {
          ...state[userId],
          team_members: [...state[userId].team_members, teamMemberId],
        },
      };

    case teamMemberActions.DELETE_TEAM_MEMBER_SUCCESS:
      teamMemberId = action.payload.teamMemberId;
      console.log(state);
      console.log(teamMemberId);
      return {
        ...Object.keys(state)
          .reduce((acc, id) => {
            const index = state[id].team_members.findIndex(e => e == teamMemberId);
            if (index === -1)
              return acc;
            return {
              ...acc,
              [id]: {
                ...state[id],
                team_members: [
                  ...state[id].team_members.slice(0, index),
                  ...state[id].team_members.slice(index+1),
                ],
              }
            };
          }, state),
      }
    case teamActions.DELETE_TEAM_SUCCESS:
      console.log(action.payload);
      let deletedTeamMembers = Object.keys(action.payload.entities.team_members);
      let usersId = Object.keys(action.payload.entities.users);
      return usersId.reduce((acc, userId) => {
        return {
          ...acc,
          [userId]: {
            ...state[userId],
            team_members: state[userId].team_members
              .filter(e => deletedTeamMembers.findIndex(tm => tm == e) === -1),
          },
        };
      }, state);

    case userActions.LOGOUT :
      return initialState;

    default:
      return state;
  }
}

export const getUser = (id, state) => {
  return denormalize(id, userSchema, state);
}

export default users;