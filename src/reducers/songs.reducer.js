import * as actions from '../actions/songs.action';
import { denormalize } from 'normalizr';
import { songSchema } from '../sagas/api/schemas';

const initialState = {
  list: {},
  songs: {},
  current: {
    song: null,
    pending: false,
    error: null,
  },
  pending: false,
  error: null,
};

function requestAction(state) {
  return {
    ...state,
    pending: true,
    error: null,
  };
}

function failureAction(state, action) {
  return {
    ...state,
    pending: false,
    error: action.error,
  }
}

function songs(state=initialState, action) {
  switch (action.type) {

    case actions.FETCH_SONGS_REQUEST:
    case actions.POST_SONG_REQUEST:
    case actions.UPDATE_SONG_REQUEST:
      return requestAction(state);

    case actions.FETCH_SONG_FAILURE:
    case actions.POST_SONG_FAILURE:
    case actions.UPDATE_SONG_FAILURE:
      return failureAction(state, action);

    case actions.FETCH_SONGS_SUCCESS:
      return {
        ...state,
        pending: false,
        error: null,
        songs: {
          ...state.songs,
          ...action.payload.songs,
        },
      };
    
    case actions.FETCH_SONG_SUCCESS:
      const {id, entities} = action.payload;
      return {
        ...state,
        pending: false,
        error: null,
        currentSong: id,
        songs: {
          ...state.songs,
          ...entities.songs
        }
      };

    case actions.POST_SONG_SUCCESS:
      return {
        ...state,
        pending: false,
        error: null,
        songs: {
          ...state.songs,
          ...action.payload.songs,
        },
      };
    
    case actions.UPDATE_SONG_SUCCESS:
      return {
        ...state,
        pending: false,
        error: null,
        songs: {
          ...state.songs,
          ...action.payload.songs,
        }
      };

    default:
      return state;
  }
}

export const getSong = (id, state) => {
  console.log(id);
  if (state.songs[id])
    console.log(denormalize(id, songSchema, state));
  return null;
}

// Return all songs ordered by name
export const getAllSongs = (state) => 
  Object.keys(state.songs)
    .map(id => state.songs[id])
    .sort((a, b) => a.title.toLowerCase() > b.title.toLowerCase());

export const getCurrentSong = (state) => getSong(state.currentSong, state);

export const isPending = (state) => state.pending;

export default songs;