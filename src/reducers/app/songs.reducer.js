import * as actions from '../../actions/songs.action';
import { getSong } from '../entities/songs.reducer';
import { LOGOUT } from '../../actions/users.action';

const initalState = {
  pending: false,
  error: null,
  currentSong: undefined,
};

function requestAction(state, action) {
  return {
    ...state,
    pending: true,
    error: null,
  };
}

function failureAction(state, action) {
  return {
    ...state,
    pending:false,
    error: action.error
  };
}

function songs(state=initalState, action) {
  switch(action.type) {

    /************************
     *       REQUESTS
     ************************/
    case actions.FETCH_SONG_REQUEST:
    case actions.FETCH_SONGS_REQUEST:
    case actions.POST_SONG_REQUEST:
    case actions.UPDATE_SONG_REQUEST:
      return requestAction(state, action);
    

    /************************
     *       FAILURES
     ************************/
    case actions.FETCH_SONG_FAILURE:
    case actions.FETCH_SONGS_FAILURE:
    case actions.POST_SONG_FAILURE:
    case actions.UPDATE_SONG_FAILURE:
      return failureAction(state, action);
    

    /************************
     *       SUCCESS
     ************************/
    case actions.FETCH_SONGS_SUCCESS:
    case actions.POST_SONG_SUCCESS:
    case actions.UPDATE_SONG_SUCCESS:
      return {
        ...state,
        pending: false,
        error: null,
      };
    case actions.FETCH_SONG_SUCCESS:
      return {
        ...state,
        pending: false,
        error: null,
        currentSong: action.payload.id,
      };

    case LOGOUT : 
      return initalState;

    default:
      return state;
  }
}

export function getCurrentSong(state, entitiesState) {
  const id = state.currentSong;
  if (id === undefined)
    return null;
  return getSong(id, entitiesState);
}

export function isPending(state) {
  return state.pending;
}

export default songs;