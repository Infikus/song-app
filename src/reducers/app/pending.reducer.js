import * as teams from '../../actions/teams.action';
import * as teamMembers from '../../actions/teamMembers.action';
import * as songs from '../../actions/songs.action';
import * as songLists from '../../actions/songLists.action';
import * as users from '../../actions/users.action';

const initialState = {};

function pending(state=initialState, action) {
    const requestRegex = /(.*)_REQUEST/;
    const successRegex = /(.*)_SUCCESS/;
    const failureRegex = /(.*)_FAILURE/;
    let actionName = null;

    if (actionName = action.type.match(requestRegex)) {
        return {
            ...state,
            [actionName[1]]: {
                pending: true,
                error: null,
                success: false,
            }
        }
    }
    if (actionName = action.type.match(successRegex)) {
        return {
            ...state,
            [actionName[1]]: {
                pending: false,
                error: null,
                success: true,
            }
        }
    }
    if (actionName = action.type.match(failureRegex)) {
        return {
            ...state,
            [actionName[1]]: {
                pending: false,
                error: action.payload.error,
                success: false,
            }
        }
    }

    if (users.LOGOUT) {
        return initialState;
    }

    return state;
}

export function isPending(action, state) {
    const actionState = state[action];
    return actionState ? actionState.pending : false;
}

export function isSuccessful(action, state) {
    const actionState = state[action];
    return actionState ? actionState.success : false;
}

export function isError(action, state) {
    const actionState = state[action];
    return actionState ? actionState.error : null;
}

export default pending;