import { ME_SUCCESS, ME_REQUEST, ME_FAILURE, REGISTER_SUCCESS, REGISTER_FAILURE, REGISTER_REQUEST, LOGIN_REQUEST, LOGIN_SUCCESS, REFRESH_TOKEN_REQUEST, ADD_WAITING_ACTION, REFRESH_TOKEN_SUCCESS, LOGIN_FAILURE, LOGOUT } from '../../actions/users.action';
import { getUser } from '../entities/users.reducer';
import { getTeam, getTeamSongs, getTeamSongLists} from '../entities/teams.reducer';
import { SET_SELECTED_TEAM, POST_TEAM_SUCCESS } from '../../actions/teams.action';
import { isInString } from '../../utils/search';

function getInitialSelectedTeam() {
  return parseInt(localStorage.getItem("selectedTeam"));
}

const initialState = {
  userId: undefined,
  selectedTeam: getInitialSelectedTeam(),
  pending: false,

  isTokenRefreshing: false,
  waitingActions: [],

  registerSuccess: false,
};

function updateSelectedTeam(state, selectedTeam) {
  // Update only if currentSelectedTeam is null
  return state.selectedTeam ? state.selectedTeam : selectedTeam;
}

function me(state=initialState, action) {
  switch(action.type) {
    case ME_SUCCESS:
      const { userId, entities } = action.payload;
      const selectedTeamMember = entities.users[userId].last_team_member;
      const selectedTeamId = entities.team_members[selectedTeamMember].team;
      return {
        ...state,
        userId: action.payload.userId,
        selectedTeam: updateSelectedTeam(state, selectedTeamId),
      };

    case SET_SELECTED_TEAM:
    case POST_TEAM_SUCCESS:
      return {
        ...state,
        selectedTeam: action.payload.teamId,
      };

    case LOGIN_SUCCESS:
      return {
        ...state,
        selectedTeam: null,
      }

    // Refresh token handling
    case REFRESH_TOKEN_REQUEST:
      return {
        ...state,
        isTokenRefreshing: true,
        waitingActions: [],
      };
    
    case ADD_WAITING_ACTION:
      return {
        ...state,
        waitingActions: [...state.waitingActions, action.payload.action],
      };

    case LOGIN_FAILURE: {
      return {
        ...state,
        isTokenRefreshing: false,
        waitingActions: [],
      }
    }

    case REFRESH_TOKEN_SUCCESS:
      return {
        ...state,
        isTokenRefreshing: false,
        waitingActions: [],
      }

    case LOGOUT:
      return initialState;

    default:
      return state;
  }
}


export const FILTER_TITLE_DESC = 'FILTER_TITLE_DESC';
export const FILTER_TITLE_ASC = 'FILTER_TITLE_ASC';
export const FILTER_AUTHOR_DESC = 'FILTER_AUTHOR_DESC';
export const FILTER_AUTHOR_ASC = 'FILTER_AUTHOR_ASC';
export const FILTER_KEY_DESC = 'FILTER_KEY_DESC';
export const FILTER_KEY_ASC = 'FILTER_KEY_ASC';
export const FILTER_BPM_DESC = 'FILTER_BPM_DESC';
export const FILTER_BPM_ASC = 'FILTER_BPM_ASC';
export const FILTER_DATE_ASC = 'FILTER_DATE_ASC';
export const FILTER_DATE_DESC = 'FILTER_DATE_DESC';
export const filterToString = {
  [FILTER_TITLE_ASC]: 'Nom - croissant',
  [FILTER_TITLE_DESC]: 'Nom - décroissant',
  [FILTER_AUTHOR_ASC]: 'Auteur - croissant',
  [FILTER_AUTHOR_DESC]: 'Auteur - décroissant',
  [FILTER_KEY_ASC]: 'Tonalité - croissant',
  [FILTER_KEY_DESC]: 'Tonalité - décroissant',
  [FILTER_BPM_ASC]: 'BPM - croissant',
  [FILTER_BPM_DESC]: 'BPM - décroissant',
  [FILTER_DATE_ASC]: 'Date - croissant',
  [FILTER_DATE_DESC]: 'Date - décroissant',
};
const filterFunctions = {
  FILTER_TITLE_ASC: (a, b) => a.title.toLowerCase().localeCompare(b.title.toLowerCase()),
  FILTER_TITLE_DESC: (a, b) => -filterFunctions[FILTER_TITLE_ASC](a, b),
  FILTER_KEY_ASC: (a, b) => {
    if (a.key - b.key !== 0) return a.key - b.key;
    return filterFunctions[FILTER_TITLE_ASC](a, b);
  },
  FILTER_KEY_DESC: (a, b) => {
    if (b.key - a.key !== 0) return b.key - a.key;
    return filterFunctions[FILTER_TITLE_ASC](a, b);
  },
  FILTER_AUTHOR_ASC: (a, b) => {
    if (a.author && b.author) return a.author.toLowerCase().localeCompare(b.author.toLowerCase());
    else if (a.author) return 1;
    else if (b.author) return -1;
    return filterFunctions[FILTER_TITLE_ASC](a, b);
  },
  FILTER_AUTHOR_DESC: (a, b) => {
    if (a.author && b.author) return b.author.toLowerCase().localeCompare(a.author.toLowerCase());
    else if (a.author) return -1;
    else if (b.author) return 1;
    return filterFunctions[FILTER_TITLE_ASC](a, b);
  },
  FILTER_BPM_ASC: (a, b) => {
    if (a.bpm && b.bpm) return a.bpm - b.bpm;
    else if (a.author) return 1;
    else if (b.author) return -1;
    return filterFunctions[FILTER_TITLE_ASC](a, b);
  },
  FILTER_BPM_DESC: (a, b) => {
    if (a.bpm && b.bpm) return b.bpm - a.bpm;
    else if (a.author) return 1;
    else if (b.author) return -1;
    return filterFunctions[FILTER_TITLE_ASC](a, b);
  },
}

export function getMe(state, entitiesState) {
  return getUser(state.userId, entitiesState);
}
export function getMyTeams(state, entitiesState) {
  const me = getMe(state, entitiesState)
  if (!me) return [];
  return me.team_members.map(tm => tm.team);
}
export function getSelectedTeamId(state) {
  return state.selectedTeam;
}
export function getSelectedTeam(state, entitiesState) {
  return getTeam(state.selectedTeam, entitiesState);
}
export function getSelectedTeamSongs(state, entitiesState) {
  return getTeamSongs(state.selectedTeam, entitiesState);
}
export function getSelectedTeamSongsPaginate(page, pageSize, search, filter, state, entitiesState) {
  const s = search.toLowerCase();
  const songs =  getTeamSongs(state.selectedTeam, entitiesState)
    .filter(song => s !== '' ? isInString(song.title, s) : true);

  return {
    count: songs.length,
    data: songs
      .sort(filterFunctions[filter])
      .filter((song, id) => Math.floor(id/pageSize) === page),
  }
}
export function getSelectedTeamSongLists(state, entitiesState) {
  return getTeamSongLists(state.selectedTeam, entitiesState);
}
/**
 * Return the role of the user if he is in the team, else -1
 */
export function getRoleInTeam(teamId, state, entitiesState) {
  const userId = state.userId;
  const teamMemberId = Object.keys(entitiesState.team_members)
    .find(id => entitiesState.team_members[id].user == userId && entitiesState.team_members[id].team == teamId);
  if (teamMemberId === undefined)
    return -1;
  const teamMember = entitiesState.team_members[teamMemberId]
  return teamMember.role;
}

export function isTokenRefreshing(state) {
  return state.isTokenRefreshing;
}
export function getWaitingActions(state) {
  return state.waitingActions;
}

export default me;