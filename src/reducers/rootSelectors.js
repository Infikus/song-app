import * as songsEntityReducer from './entities/songs.reducer';
import * as songsAppReducer from './app/songs.reducer';
import * as meAppReducer from './app/me.reducer';
import * as teamsEntityReducer from './entities/teams.reducer';
import * as teamMembersEntityReducer from './entities/teamMembers.reducer';
import * as songListsEntityReducer from './entities/songLists.reducer';
import * as pendingAppReducer from './app/pending.reducer';

export const isPending = (action, state) => pendingAppReducer.isPending(action, state.app.pending);
export const isSuccessful = (action, state) => pendingAppReducer.isSuccessful(action, state.app.pending);
export const isError = (action, state) => pendingAppReducer.isError(action, state.app.pending);

export const getTeam = (id, state) => teamsEntityReducer.getTeam(id, state.entities);

export const getTeamMember = (id, state) => teamMembersEntityReducer.getTeamMember(id, state.entities);

export const getSong = (id, state) => songsEntityReducer.getSong(id, state.entities);
export const getAllSongs = (state) => songsEntityReducer.getAllSongs(state.entities);

export const getSongList = (id, state, denorm=true) => songListsEntityReducer.getSongList(id, state.entities, denorm);
export const getSongsOfSongList = (id, state) => songListsEntityReducer.getSongsOfSongList(id, state.entities);

export const getCurrentSong = (state) => songsAppReducer.getCurrentSong(state.app.songs, state.entities);
export const isSongPending = (state) => songsAppReducer.isPending(state.app.songs);

export const getMe = (state) => meAppReducer.getMe(state.app.me, state.entities);
export const getMyTeams = (state) => meAppReducer.getMyTeams(state.app.me, state.entities);
export const getMyRoleInTeam = (teamId, state) => meAppReducer.getRoleInTeam(teamId, state.app.me, state.entities);
export const getSelectedTeam = (state) => meAppReducer.getSelectedTeam(state.app.me, state.entities);
export const getSelectedTeamId = (state) => meAppReducer.getSelectedTeamId(state.app.me);
export const getSelectedTeamSongs = (state) => meAppReducer.getSelectedTeamSongs(state.app.me, state.entities);
export const getSelectedTeamSongsPaginate = (page, pageSize, search, filter, state) =>
  meAppReducer.getSelectedTeamSongsPaginate(page, pageSize, search, filter, state.app.me, state.entities);
export const getSelectedTeamSongLists = (state) => meAppReducer.getSelectedTeamSongLists(state.app.me, state.entities);
export const isMePending = (state) => meAppReducer.isPending(state.app.me);
export const isTokenRefreshing = (state) => meAppReducer.isTokenRefreshing(state.app.me);
export const getWaitingActions = (state) => meAppReducer.getWaitingActions(state.app.me);
