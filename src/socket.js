import * as actions from './actions/socket.actions';

const wsServer = 'ws://localhost:5000';

class Socket {
  constructor(dispatch) {
    this.dispatch = dispatch;

    this.subscribe = this.subscribe.bind(this);
    this.connect = this.connect.bind(this);
    this.send = this.send.bind(this);
  }

  connect(url, onopen=null) {
    this.url = url;
    this.socket = new WebSocket(url);
    this.hasSubsribed = false;
    this.userId = null;

    this.socket.onopen = () => {
      console.log('Socket opened');
      if (onopen) {
        onopen(this);
      }
    }
    
    this.socket.onmessage = (event) => {
      const data = JSON.parse(event.data);
      console.log("onmessage", data);
      if (data && data.type) {
        // Check if the type is valid
        const isTypeValid = actions.allowedActions.indexOf(data.type) >= 0;
        if (isTypeValid) {
          this.dispatch(data);
        }
      }
    }
  }

  subscribe(userId) {
    if (!this.hasSubscribe || this.userId !== userId) {
      this.send(JSON.stringify(actions.subscribe(userId)));
      this.hasSubscribe = true;
      this.userId = userId;
    }
  }

  send(data) {
    this.socket.send(data);
  }
}

const configureSocket = (dispatch, userId = null) => {
  const socket = new Socket(dispatch);

  socket.connect(wsServer, socket => {
    if (userId !== null) {
      socket.subscribe(userId);
    }
  })

  return socket;
}

export default configureSocket;